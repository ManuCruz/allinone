#ifndef _INPUT_MANAGER_H_
#define _INPUT_MANAGER_H_

#include "string.h"
#include "array.h"
#include <map>

#define ZONA_MUERTA 0.1

// c�digos que representan los distintos tipos de inputs posibles
#define KEYBOARD_OFFSET 0
#define MOUSE_OFFSET 1000
#define PAD_OFFSET 2000
enum eInputCode{
  //keyboard
  KEY_SPACE = KEYBOARD_OFFSET + 32,
  KEY_APOSTROPHE = KEYBOARD_OFFSET + 39,
  KEY_COMMA = KEYBOARD_OFFSET + 44,
  KEY_MINUS = KEYBOARD_OFFSET + 45,
  KEY_PERIOD = KEYBOARD_OFFSET + 46,
  KEY_SLASH = KEYBOARD_OFFSET + 47,
  KEY_0 = KEYBOARD_OFFSET + 48,
  KEY_1 = KEYBOARD_OFFSET + 49,
  KEY_2 = KEYBOARD_OFFSET + 50,
  KEY_3 = KEYBOARD_OFFSET + 51,
  KEY_4 = KEYBOARD_OFFSET + 52,
  KEY_5 = KEYBOARD_OFFSET + 53,
  KEY_6 = KEYBOARD_OFFSET + 54,
  KEY_7 = KEYBOARD_OFFSET + 55,
  KEY_8 = KEYBOARD_OFFSET + 56,
  KEY_9 = KEYBOARD_OFFSET + 57,
  KEY_SEMICOLON = KEYBOARD_OFFSET + 59,
  KEY_EQUAL = KEYBOARD_OFFSET + 61,
  KEY_A = KEYBOARD_OFFSET + 65,
  KEY_B = KEYBOARD_OFFSET + 66,
  KEY_C = KEYBOARD_OFFSET + 67,
  KEY_D = KEYBOARD_OFFSET + 68,
  KEY_E = KEYBOARD_OFFSET + 69,
  KEY_F = KEYBOARD_OFFSET + 70,
  KEY_G = KEYBOARD_OFFSET + 71,
  KEY_H = KEYBOARD_OFFSET + 72,
  KEY_I = KEYBOARD_OFFSET + 73,
  KEY_J = KEYBOARD_OFFSET + 74,
  KEY_K = KEYBOARD_OFFSET + 75,
  KEY_L = KEYBOARD_OFFSET + 76,
  KEY_M = KEYBOARD_OFFSET + 77,
  KEY_N = KEYBOARD_OFFSET + 78,
  KEY_O = KEYBOARD_OFFSET + 79,
  KEY_P = KEYBOARD_OFFSET + 80,
  KEY_Q = KEYBOARD_OFFSET + 81,
  KEY_R = KEYBOARD_OFFSET + 82,
  KEY_S = KEYBOARD_OFFSET + 83,
  KEY_T = KEYBOARD_OFFSET + 84,
  KEY_U = KEYBOARD_OFFSET + 85,
  KEY_V = KEYBOARD_OFFSET + 86,
  KEY_W = KEYBOARD_OFFSET + 87,
  KEY_X = KEYBOARD_OFFSET + 88,
  KEY_Y = KEYBOARD_OFFSET + 89,
  KEY_Z = KEYBOARD_OFFSET + 90,
  KEY_LEFT_BRACKET = KEYBOARD_OFFSET + 91,
  KEY_BACKSLASH = KEYBOARD_OFFSET + 92,
  KEY_RIGHT_BRACKET = KEYBOARD_OFFSET + 93,
  KEY_GRAVE_ACCENT = KEYBOARD_OFFSET + 96,
  KEY_ESC = KEYBOARD_OFFSET + 257,
  KEY_F1 = KEYBOARD_OFFSET + 258,
  KEY_F2 = KEYBOARD_OFFSET + 259,
  KEY_F3 = KEYBOARD_OFFSET + 260,
  KEY_F4 = KEYBOARD_OFFSET + 261,
  KEY_F5 = KEYBOARD_OFFSET + 262,
  KEY_F6 = KEYBOARD_OFFSET + 263,
  KEY_F7 = KEYBOARD_OFFSET + 264,
  KEY_F8 = KEYBOARD_OFFSET + 265,
  KEY_F9 = KEYBOARD_OFFSET + 266,
  KEY_F10 = KEYBOARD_OFFSET + 267,
  KEY_F11 = KEYBOARD_OFFSET + 268,
  KEY_F12 = KEYBOARD_OFFSET + 269,
  KEY_F13 = KEYBOARD_OFFSET + 270,
  KEY_F14 = KEYBOARD_OFFSET + 271,
  KEY_F15 = KEYBOARD_OFFSET + 272,
  KEY_F16 = KEYBOARD_OFFSET + 273,
  KEY_F17 = KEYBOARD_OFFSET + 274,
  KEY_F18 = KEYBOARD_OFFSET + 275,
  KEY_F19 = KEYBOARD_OFFSET + 276,
  KEY_F20 = KEYBOARD_OFFSET + 277,
  KEY_F21 = KEYBOARD_OFFSET + 278,
  KEY_F22 = KEYBOARD_OFFSET + 279,
  KEY_F23 = KEYBOARD_OFFSET + 280,
  KEY_F24 = KEYBOARD_OFFSET + 281,
  KEY_F25 = KEYBOARD_OFFSET + 282,
  KEY_UP = KEYBOARD_OFFSET + 283,
  KEY_DOWN = KEYBOARD_OFFSET + 284,
  KEY_LEFT = KEYBOARD_OFFSET + 285,
  KEY_RIGHT = KEYBOARD_OFFSET + 286,
  KEY_LSHIFT = KEYBOARD_OFFSET + 287,
  KEY_RSHIFT = KEYBOARD_OFFSET + 288,
  KEY_LCTRL = KEYBOARD_OFFSET + 289,
  KEY_RCTRL = KEYBOARD_OFFSET + 290,
  KEY_LALT = KEYBOARD_OFFSET + 291,
  KEY_RALT = KEYBOARD_OFFSET + 292,
  KEY_TAB = KEYBOARD_OFFSET + 293,
  KEY_ENTER = KEYBOARD_OFFSET + 294,
  KEY_BACKSPACE = KEYBOARD_OFFSET + 295,
  KEY_INSERT = KEYBOARD_OFFSET + 296,
  KEY_DEL = KEYBOARD_OFFSET + 297,
  KEY_PAGEUP = KEYBOARD_OFFSET + 298,
  KEY_PAGEDOWN = KEYBOARD_OFFSET + 299,
  KEY_HOME = KEYBOARD_OFFSET + 300,
  KEY_END = KEYBOARD_OFFSET + 301,
  KEY_KP_0 = KEYBOARD_OFFSET + 302,
  KEY_KP_1 = KEYBOARD_OFFSET + 303,
  KEY_KP_2 = KEYBOARD_OFFSET + 304,
  KEY_KP_3 = KEYBOARD_OFFSET + 305,
  KEY_KP_4 = KEYBOARD_OFFSET + 306,
  KEY_KP_5 = KEYBOARD_OFFSET + 307,
  KEY_KP_6 = KEYBOARD_OFFSET + 308,
  KEY_KP_7 = KEYBOARD_OFFSET + 309,
  KEY_KP_8 = KEYBOARD_OFFSET + 310,
  KEY_KP_9 = KEYBOARD_OFFSET + 311,
  KEY_KP_DIVIDE = KEYBOARD_OFFSET + 312,
  KEY_KP_MULTIPLY = KEYBOARD_OFFSET + 313,
  KEY_KP_SUBTRACT = KEYBOARD_OFFSET + 314,
  KEY_KP_ADD = KEYBOARD_OFFSET + 315,
  KEY_KP_DECIMAL = KEYBOARD_OFFSET + 316,
  KEY_KP_EQUAL = KEYBOARD_OFFSET + 317,
  KEY_KP_ENTER = KEYBOARD_OFFSET + 318,
  KEY_KP_NUM_LOCK = KEYBOARD_OFFSET + 319,
  KEY_CAPS_LOCK = KEYBOARD_OFFSET + 320,
  KEY_SCROLL_LOCK = KEYBOARD_OFFSET + 321,
  KEY_PAUSE = KEYBOARD_OFFSET + 322,
  KEY_LSUPER = KEYBOARD_OFFSET + 323,
  KEY_RSUPER = KEYBOARD_OFFSET + 324,
  KEY_MENU = KEYBOARD_OFFSET + 325,

  //mouse
  MOUSE_1 = MOUSE_OFFSET + 0,
  MOUSE_2 = MOUSE_OFFSET + 1,
  MOUSE_3 = MOUSE_OFFSET + 2,
  MOUSE_4 = MOUSE_OFFSET + 3,
  MOUSE_5 = MOUSE_OFFSET + 4,
  MOUSE_6 = MOUSE_OFFSET + 5,
  MOUSE_7 = MOUSE_OFFSET + 6,
  MOUSE_8 = MOUSE_OFFSET + 7,

  //pad
  PAD_DPAD_UP = PAD_OFFSET + 0,
  PAD_DPAD_DOWN = PAD_OFFSET + 1,
  PAD_DPAD_LEFT = PAD_OFFSET + 2,
  PAD_DPAD_RIGHT = PAD_OFFSET + 3,
  PAD_START = PAD_OFFSET + 4,
  PAD_BACK = PAD_OFFSET + 5,
  PAD_LEFT_THUMB = PAD_OFFSET + 6,
  PAD_RIGHT_THUMB = PAD_OFFSET + 7,
  PAD_LEFT_SHOULDER = PAD_OFFSET + 8,
  PAD_RIGHT_SHOULDER = PAD_OFFSET + 9,
  PAD_A = PAD_OFFSET + 10,
  PAD_B = PAD_OFFSET + 11,
  PAD_X = PAD_OFFSET + 12,
  PAD_Y = PAD_OFFSET + 13,
  PAD_LTRIGGER = PAD_OFFSET + 14,
  PAD_RTRIGGER = PAD_OFFSET + 15,
  PAD_THUMB_LX = PAD_OFFSET + 16,
  PAD_THUMB_LY = PAD_OFFSET + 17,
  PAD_THUMB_RX = PAD_OFFSET + 18,
  PAD_THUMB_RY = PAD_OFFSET + 19,
};

enum eState{
  pressed,
  down,
  up,
  notPressed,
};

// estructura que representa una acci�n
struct Action{
  Array<eInputCode> inputCodes;
  eState prevState = notPressed;
  eState state = notPressed;
};

const float STEP = 0.10f;
const float MAX_ABS = 1;
struct ActionAxis{
  Array<eInputCode> inputCodesPos;
  Array<eInputCode> inputCodesNeg;
  float value = 0;
  float prevValue = 0;
};

enum eRotor{
  rLeft,
  rRight,
};

class InputManager{
public:
  static InputManager& Instance();

  // Inicialici�n: deteccci�n de dispostivos, inicializaci�n de los mismos... etc
  bool Init();
  // Cierre
  void End();
  // Devuelve true si el manager ha sido inicializado correctamente
  bool IsOk();

  // Funci�n de actualizaci�n, actualizaci�n de estados entre frames de las acciones
  void Update();

  // Crea un bot�n virtual
  void CreateAction(const String& name, eInputCode button);
  // Crea un eje virtual
  void CreateActionAxis(const String& name, eInputCode positiveAxis, eInputCode negativeAxis);

  // Est� el bot�n pulsado en este momento?
  bool IsActionPressed(const String& name) const;
  // Devuelve true durante el frame que que el usuario ha comenzaco la pulsaci�n de un bot�n
  bool IsActionDown(const String& name) const;
  // Devuelve true durante el frame que que el usuario ha dejado de pulsar un bot�n
  bool IsActionUp(const String& name) const;

  // Estado de ejes virtuales normalizado de -1 a +1
  float GetActionAxis(const String& name) const;

  void setVibration(eRotor rotor, uint32 value);
  uint32 getVibration(eRotor rotor);
protected:
  InputManager();
  ~InputManager();

  float normalize(float value, float max, float min);

private:
  static InputManager* inputManager;

  bool valid;
  uint32 powerLeft;
  uint32 powerRight;

  std::map<String, Action> dicAction;
  std::map<String, ActionAxis> dicActionAxis;
};

#endif
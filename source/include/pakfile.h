#ifndef UGINE_PAKFILE_H
#define UGINE_PAKFILE_H

#include "array.h"
#include "string.h"
#include "types.h"
#include <stdio.h>

struct PakEntry {
  String mFilename;
  uint32 mOffset;
  uint32 mSize;

  PakEntry() {}
  PakEntry(const String& filename, uint32 offset, uint32 size) {
    mFilename = filename;
    mOffset = offset;
    mSize = size;
  }
};

class PakFile {
public:
  PakFile(const String& filename);
  virtual ~PakFile() { if ( mHandle ) fclose(mHandle); }

  bool IsValid() const { return mHandle != NULL; }

  bool FileExists(const String& filename) const { return FileEntry(filename) != NULL; }
  uint32 FileSize(const String& filename) const;
  bool GetFile(const String& filename, void* buffer) const;

protected:
  const PakEntry* FileEntry(const String& filename) const;

private:
  Array<PakEntry> mEntries;
  FILE* mHandle;
};

inline PakFile::PakFile(const String& filename) {
  mHandle = fopen(filename.ToCString(), "rb");
  if ( mHandle ) {
    // Read number of files
    uint32 numFiles;
    fread(&numFiles, sizeof(uint32), 1, mHandle);

    // Read each file
    for ( uint32 i = 0; i < numFiles; i++ ) {
      // Read filename
      String name;
      uint32 strLen;
      fread(&strLen, sizeof(uint32), 1, mHandle);
      char* strBuffer = (char*)malloc(strLen+1);
      fread(strBuffer, strLen, 1, mHandle);
      strBuffer[strLen] = 0;
      name = strBuffer;
      free(strBuffer);

      // Read offset
      uint32 offset;
      fread(&offset, sizeof(uint32), 1, mHandle);

      // Read size
      uint32 size;
      fread(&size, sizeof(uint32), 1, mHandle);

      mEntries.Add(PakEntry(name, offset, size));
    }
  }
}

inline uint32 PakFile::FileSize(const String& filename) const {
  const PakEntry* pakEntry = FileEntry(filename);
  if ( pakEntry )
    return pakEntry->mSize;
  return 0;
}

inline bool PakFile::GetFile(const String& filename, void* buffer) const {
  const PakEntry* pakEntry = FileEntry(filename);
  if ( pakEntry) {
    fseek(mHandle, pakEntry->mOffset, SEEK_SET);
    fread(buffer, pakEntry->mSize, 1, mHandle);
    return true;
  }
  return false;
}

inline const PakEntry* PakFile::FileEntry(const String& filename) const {
  for ( uint32 i = 0; i < mEntries.Size(); i++ )
    if ( mEntries[i].mFilename == filename ) 
      return &mEntries[i];
  return NULL;
}

#endif

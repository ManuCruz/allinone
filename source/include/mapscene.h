#ifndef UGINE_MAPSCENE_H
#define UGINE_MAPSCENE_H

#include "types.h"
#include "parallaxscene.h"
#include "map.h"

class Image;

class MapScene : public ParallaxScene {
public:
  MapScene(Map* map, Image* imageBack = NULL, Image* imageFront = NULL) : ParallaxScene(imageBack, imageFront) { this->map = map; }
  virtual ~MapScene() {}

  virtual const Map* GetMap() const { return map; }
  virtual void Update(double elapsed) { ParallaxScene::Update(elapsed, map); }

protected:
  virtual void RenderAfterBackground() const { map->Render(); }

private:
  Map* map;
};

#endif

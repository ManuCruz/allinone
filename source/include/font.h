#ifndef UGINE_FONT_H
#define UGINE_FONT_H

#include "string.h"
#include "types.h"
#include "image.h"
#include "glyph.h"

class Font{
public:
  Font(){};
  Font(const String& filename);
  Font(const String& filename, uint16 fontSize);
  virtual ~Font();
  virtual uint16 GetSize() const;
  virtual uint32 GetTextWidth(const String& text) const;
  virtual uint32 GetTextHeight(const String& text) const;
  virtual float GetTextSubHeight(const String& text) const;
  virtual void Render(const String& text, double x, double y) const;

  virtual bool IsValid() const { return images[0]->IsValid(); }
  virtual const String& GetFilename() const { return images[0]->GetFilename(); }

private:
  Array<Image *> images;
  Array<Glyph> glifos;
  uint16 fontSize;
};

#endif
#include "../include/scene.h"
#include "../include/emitter.h"
#include "../include/image.h"
#include "../include/screen.h"
#include "../include/sprite.h"

Scene::Scene(Image* backgroundImg) {
  SetBackgroundImage(backgroundImg);
  SetBackgroundColor(0, 0, 0);
}

Scene::~Scene() {
  uint32 sizeSprites;
  uint32 sizeEmitters;
  for (uint32 i = 0; i < LAYER_COUNT; i++) {
    sizeSprites = sprites[i].Size();
    for (uint32 j = 0; j < sizeSprites; j++)
      delete sprites[i][j];
    sizeEmitters = emitters[i].Size();
    for ( uint32 j = 0; j < sizeEmitters; j++ ) 
      delete emitters[i][j];
  }
}

Sprite* Scene::CreateSprite(Image* image, Layer layer) {
  Sprite* spr = new Sprite(image);
  AddSprite(spr, layer);
  return spr;
}

void Scene::DeleteSprite(Sprite* sprite) {
  for (uint32 i = 0; i < LAYER_COUNT; i++)
    sprites[i].Remove(sprite);
  delete sprite;
}

Emitter* Scene::CreateEmitter(Image* image, bool autofade, Layer layer) {
  emitters[layer].Add(new Emitter(image, autofade));
  return emitters[layer].Last();
}

void Scene::DeleteEmitter(Emitter* emitter) {
  for ( int i = 0; i < LAYER_COUNT; i++ )
    emitters[i].Remove(emitter);
  delete emitter;
}

void Scene::Update(double elapsed, Map* map) {
  // Actualizamos sprites y emitters
  uint32 sizeSprites;
  uint32 sizeEmitters;
  for (uint32 i = 0; i < LAYER_COUNT; i++) {
    sizeSprites = sprites[i].Size();
    for (uint32 j = 0; j < sizeSprites; j++)
      sprites[i][j]->Update(elapsed, map);
    sizeEmitters = emitters[i].Size();
    for ( uint32 j = 0; j < sizeEmitters; j++ ) 
      emitters[i][j]->Update(elapsed);
  }

  // Actualizamos colisiones
  int sizeS;
  for ( int32 i = 0; i < LAYER_COUNT; i++ ){
    sizeS = sprites[i].Size();
    for (int32 j = 0; j < sizeS - 1; j++)
      for (int32 k = j + 1; k < sizeS; k++)
        sprites[i][j]->CheckCollision(sprites[i][k]);
  }

  // Actualizamos camara
  camera.Update();
}

void Scene::Render() const {
  Renderer::Instance().Clear(backr, backg, backb);
  Renderer::Instance().SetOrigin(GetCamera().GetX(), GetCamera().GetY());
  if ( backgroundImg ) 
    Renderer::Instance().DrawImage(backgroundImg, 0., 0.);
  Renderer::Instance().SetOrigin(0., 0.);
  RenderBackground();
  Renderer::Instance().SetOrigin(GetCamera().GetX(), GetCamera().GetY());
  RenderAfterBackground();
  for (uint32 i = 0; i < LAYER_COUNT; i++) {
    RenderSprites((Layer)i);
    RenderEmitters((Layer)i);
  }
}

void Scene::RenderSprites(Layer layer) const {
  uint32 size = sprites[layer].Size();
  for ( uint32 i = 0; i < size; i++ ) 
    sprites[layer][i]->Render();
}

void Scene::RenderEmitters(Layer layer) const {
  uint32 size = emitters[layer].Size();
  for ( uint32 i = 0; i < size; i++ ) 
    emitters[layer][i]->Render();
}

#include "../include/isometricmap.h"
#include "../include/collision.h"
#include "../include/image.h"
#include "../include/resourcemanager.h"
#include "../include/isometricscene.h"
#include "../include/isometricsprite.h"
#include "../include/math.h"
#include "../include/base64.h"
#include "../include/filemanager.h"
#include "../lib/rapidxml.hpp"

using namespace rapidxml;

IsometricMap::IsometricMap(const String& filename, uint16 firstColId) : Map(filename, firstColId) {
  SetValid(false);

  String file = FileManager::Instance().LoadString(filename);

  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* nTileset = doc.first_node("map")->first_node("tileset");
    int32 firstgid = String(nTileset->first_attribute("firstgid")->value()).ToInt();

    xml_node<>* nData = doc.first_node("map")->first_node("layer")->next_sibling("layer")->first_node("data");

    //si no hay atributos “encoding” o “compression”,
    if (!nData->first_attribute("encoding") && !nData->first_attribute("compression")){
      xml_node<>* nTile = nData->first_node("tile");
      while (nTile){
        topLayerIds.Add(String(nTile->first_attribute("gid")->value()).ToInt() - firstgid);
        nTile = nTile->next_sibling("tile");
      }

      if (GetImage()){
        GetImage()->SetHandle(GetImage()->GetHandleX() + GetTileWidth(), GetImage()->GetHeight() - GetImage()->GetHandleY() - GetTileHeight());
        SetValid(true);
      }
    }
    else if (String(nData->first_attribute("encoding")->value()) == "csv"){
      String cad = nData->value();
      cad = cad.Replace("\n", "");
      cad = cad.Trim();
      Array<String> aCad = cad.Split(",");
      for (uint32 i = 0; i < aCad.Size(); i++)
        topLayerIds.Add(aCad[i].ToInt() - firstgid);

      if (GetImage()){
        GetImage()->SetHandle(GetImage()->GetHandleX() + GetTileWidth(), GetImage()->GetHeight() - GetImage()->GetHandleY() - GetTileHeight());
        SetValid(true);
      }
    }
    else if (String(nData->first_attribute("encoding")->value()) == "base64" && !nData->first_attribute("compression")){
      String cad = nData->value();
      cad = cad.Replace("\n", "");
      cad = cad.Trim();
      cad = base64_decode(cad);

      uint32 value;
      for (int32 i = 0; i < cad.Length(); i += 4){
        value = (cad[i + 3] - 0x30);
        value = value << 8;
        value = (cad[i + 2] - 0x30);
        value = value << 8;
        value = (cad[i + 1] - 0x30);
        value = value << 8;
        value = (cad[i] - 0x30);

        topLayerIds.Add(value - firstgid);
      }

      if (GetImage()){
        GetImage()->SetHandle(GetImage()->GetHandleX() + GetTileWidth(), GetImage()->GetHeight() - GetImage()->GetHandleY() - GetTileHeight());
        SetValid(true);
      }
    }
  }
}

void IsometricMap::GenerateLayerSprites(IsometricScene* scene) {
  for (uint16 y = 0; y < GetRows(); y++)
    for (uint16 x = 0; x < GetColumns(); x++)
      if (GetLayerId(x, y) > -1){
        IsometricSprite *spr = scene->CreateSprite(GetImage(), Scene::LAYER_MIDDLE);
        spr->SetCurrentFrame(static_cast<uint16>(GetLayerId(x, y)));
        if (GetLayerId(x, y) >= GetFirstColId())
          spr->SetCollision(Sprite::COLLISION_RECT);
        spr->SetPosition(x*GetTileWidth(), y*GetTileHeight(), 0.);
        spr->SetCollisionSize(GetTileWidth(), GetTileWidth());
      }
}

uint16 IsometricMap::GetTileWidth() const {
  return static_cast<uint16>(Map::GetTileWidth() / 2.);
}

void IsometricMap::Render() const {
  double scrX, scrY;
  for (uint16 y = 0; y < GetRows(); y++)
    for (uint16 x = 0; x < GetColumns(); x++)
      if (GetTileId(x, y) >= 0){
        TransformIsoCoords(x*GetTileWidth(), y*GetTileHeight(), 0., &scrX, &scrY);
        Renderer::Instance().DrawImage(GetImage(), scrX, scrY, GetTileId(x, y));
      }
}

int32 IsometricMap::GetLayerId(uint16 column, uint16 row) {
  return topLayerIds[row*GetColumns() + column];
}
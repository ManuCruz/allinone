#include "../include/camera.h"
#include "../include/sprite.h"
#include <math.h>

#define SCR Screen::Instance()

Camera::Camera(){
  x = 0;
  y = 0.;
  boundx0 = 0.;
  boundy0 = 0.;
  boundx1 = 0;
  boundy1 = 0;
  followingSprite = NULL;
}

Camera::~Camera(){
}

void Camera::SetPosition(double x, double y) { 
  SetX(x);
  SetY(y);
}

void Camera::SetX(double x) {
  if (HasBounds()){
    if (x < boundx0)
      this->x = boundx0;
    else if (x >(boundx1 - SCR.GetWidth()))
      this->x = boundx1 - SCR.GetWidth();
    else
      this->x = x;
  }
  else
    this->x = x;
}

void Camera::SetY(double y) { 
  if (HasBounds()){
    if (y < boundy0)
      this->y = boundy0;
    else if (y >(boundy1 - SCR.GetHeight()))
      this->y = boundy1 - SCR.GetHeight();
    else
      this->y = y;
  }
  else
    this->y = y;
}

void Camera::SetBounds(double bx0, double by0, double bx1, double by1) {
  if (bx0 < bx1){
    boundx0 = bx0;
    boundx1 = bx1;
  }
  if (by0 < by1){
    boundy0 = by0;
    boundy1 = by1;
  }
}

bool Camera::HasBounds() const { 
  return !(boundx0 == boundx1 && boundy0 == boundy1);
}

void Camera::FollowSprite(const Sprite* sprite) {
  followingSprite = sprite; 
}

void Camera::Update(){
  if (followingSprite){
    x = followingSprite->GetScreenX() - SCR.GetWidth() / 2;;
    y = followingSprite->GetScreenY() - SCR.GetHeight() / 2;;

    SetPosition(x, y);
  }
}
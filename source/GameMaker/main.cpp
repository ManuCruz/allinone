#include "../include/types.h"
#include "../include/string.h"

#include <stdio.h>
#include <conio.h>

void clear();

String newEntity();
String componentWithoutParam(String text);
String componentRenderable();
String componentPositionWithBoundary();
String componentMove(String text);
String componentKeyboardControl();

void main() {
  String fileName;
  char *strValue = new char[256];
  float dValue = 0;

  clear();

  printf("FileName(str): ");
  gets(strValue);
  fileName = strValue;

  String file = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  file += "<game ";

  printf("timeToWin(double) (zero or negative value = no time): ");
  scanf_s("%f", &dValue);
  gets(strValue);
  if (dValue>0)
    file += "timeToWin=\"" + String::FromFloat(dValue) + "\" ";

  printf("sprite for special effects emitter(str) (empty = no value): ");
  gets(strValue);
  if (String(strValue) != "")
    file += "emitter=\"" + String(strValue) + "\" ";

  file += ">\n";

  bool anotherEnt = true;;
  String ent;
  do{
    //new entity
    ent = newEntity();

    //save entity
    printf("Save this entity?(y/n): ");
    gets(strValue);
    while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
      printf("\tPlease, type 'y' or 'n': ");
      gets(strValue);
    }
    if (String(strValue).Lower() == "y")
      file += ent;

    //another entity
    printf("Another entity?(y/n): ");
    gets(strValue);
    while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
      printf("\tPlease, type 'y' or 'n': ");
      gets(strValue);
    }
    if (String(strValue).Lower() == "n")
      anotherEnt = false;

  } while (anotherEnt);

  file += "</game>";

  //save file
  file.Write(fileName, false);

  printf("Press ENTER to finish ");
  gets(strValue);
  delete strValue;
}

void clear(){
  system("cls");
  printf(" AllInOne GameMaker \n");
  printf("--------------------\n");
}

String newEntity(){
  char *strValue = new char[256];
  float dValue = 0;
  int8 inChar;

  String cad = "\t<entity>\n";

  clear();

  //component MainChar
  cad += componentWithoutParam("MainChar");

  //component Pickable
  cad += componentWithoutParam("Pickable");

  //component Enemy
  cad += componentWithoutParam("Enemy");

  //component Renderable
  cad += componentRenderable();

  //component PositionWithBoundary
  cad += componentPositionWithBoundary();

  //component Movement
  cad += componentMove("Movement");

  //component Counter
  cad += componentWithoutParam("Counter");

  //component KeyboardControl
  cad += componentKeyboardControl();

  //component Collisionable
  cad += componentWithoutParam("Collisionable");

  //component AutoMove
  cad += componentMove("AutoMove");

  //component Impenetrable
  cad += componentWithoutParam("Impenetrable");

  //component Goal
  cad += componentWithoutParam("Goal");

  cad += "\t</entity>\n";

  delete strValue;
  return cad;
}

String componentWithoutParam(String text){
  char *strValue = new char[256];
  String cad = "";

  printf("component %s?(y/n): ", text);
  gets(strValue);
  while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
    printf("\tPlease, type 'y' or 'n': ");
    gets(strValue);
  }
  if (String(strValue).Lower() == "y")
    cad += "\t\t<" + text + "/>\n";

  delete strValue;
  return cad;
}

String componentRenderable(){
  char *strValue = new char[256];
  String cad = "";

  printf("component Renderable?(y/n): ");
  gets(strValue);
  while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
    printf("\tPlease, type 'y' or 'n': ");
    gets(strValue);
  }
  if (String(strValue).Lower() == "y"){
    String sprite;
    printf("\tsprite(str): ");
    gets(strValue);
    sprite = String(strValue);

    int32 r, g, b;
    printf("\tred(0-255): ");
    scanf_s("%d", &r);
    gets(strValue);
    while (r < 0 || r > 255){
      printf("\tPlease, values between 0 and 255: ");
      scanf_s("%d", &r);
      gets(strValue);
    }

    printf("\tgreen(0-255): ");
    scanf_s("%d", &g);
    gets(strValue);
    while (g < 0 || g > 255){
      printf("\tPlease, values between 0 and 255: ");
      scanf_s("%d", &g);
      gets(strValue);
    }

    printf("\tblue(0-255): ");
    scanf_s("%d", &b);
    gets(strValue);
    while (b < 0 || b > 255){
      printf("\tPlease, values between 0 and 255: ");
      scanf_s("%d", &b);
      gets(strValue);
    }

    cad += "\t\t<Renderable sprite=\"" + sprite + "\" r=\"" + String::FromInt(r) + "\" g=\"" + String::FromInt(g) + "\" b=\"" + String::FromInt(b) + "\"/>\n";
  }

  delete strValue;
  return cad;
}

String componentPositionWithBoundary(){
  char *strValue = new char[256];
  String cad = "";

  printf("component PositionWithBoundary?(y/n): ");
  gets(strValue);
  while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
    printf("\tPlease, type 'y' or 'n': ");
    gets(strValue);
  }
  if (String(strValue).Lower() == "y"){
    float minX, maxX, minY, maxY;
    printf("\tminX(double): ");
    scanf_s("%f", &minX);
    gets(strValue);

    printf("\tmaxX(double): ");
    scanf_s("%f", &maxX);
    gets(strValue);

    printf("\tminY(double): ");
    scanf_s("%f", &minY);
    gets(strValue);

    printf("\tmaxY(double): ");
    scanf_s("%f", &maxY);
    gets(strValue);

    cad += "\t\t<PositionWithBoundary minX = \"" + String::FromFloat(minX) + "\" maxX=\"" + String::FromFloat(maxX) + "\" minY=\"" + String::FromFloat(minY) + "\" maxY=\"" + String::FromFloat(maxY) + "\" ";

    printf("\tSpecific boundaries (default limits are the edges of the screen)?(y/n): ");
    gets(strValue);
    while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
      printf("\tPlease, type 'y' or 'n': ");
      gets(strValue);
    }
    if (String(strValue).Lower() == "y"){
      float bX0, bX1, bY0, bY1;
      printf("\t\tbX0(double): ");
      scanf_s("%f", &bX0);
      gets(strValue);

      printf("\t\tbX1(double): ");
      scanf_s("%f", &bX1);
      gets(strValue);

      printf("\t\tbY0(double): ");
      scanf_s("%f", &bY0);
      gets(strValue);

      printf("\t\tbY1(double): ");
      scanf_s("%f", &bY1);
      gets(strValue);

      cad += "bX0 = \"" + String::FromFloat(bX0) + "\" bX1=\"" + String::FromFloat(bX1) + "\" bY0=\"" + String::FromFloat(bY0) + "\" bY1=\"" + String::FromFloat(bY1) + "\"";
    }

    cad += "/>\n";
  }

  delete strValue;
  return cad;
}

String componentMove(String text){
  char *strValue = new char[256];
  String cad = "";

  printf("component %s?(y/n): ", text);
  gets(strValue);
  while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
    printf("\tPlease, type 'y' or 'n': ");
    gets(strValue);
  }
  if (String(strValue).Lower() == "y"){
    float minSpeedX, maxSpeedX, minSpeedY, maxSpeedY;
    printf("\tminSpeedX(double): ");
    scanf_s("%f", &minSpeedX);
    gets(strValue);

    printf("\tmaxSpeedX(double): ");
    scanf_s("%f", &maxSpeedX);
    gets(strValue);

    printf("\tminSpeedY(double): ");
    scanf_s("%f", &minSpeedY);
    gets(strValue);

    printf("\tmaxSpeedY(double): ");
    scanf_s("%f", &maxSpeedY);
    gets(strValue);

    cad += "\t\t<" + text + " minSpeedX = \"" + String::FromFloat(minSpeedX) + "\" maxSpeedX=\"" + String::FromFloat(maxSpeedX) + "\" minSpeedY=\"" + String::FromFloat(minSpeedY) + "\" maxSpeedY=\"" + String::FromFloat(maxSpeedY) + "\"/>\n";
  }

  delete strValue;
  return cad;
}

String componentKeyboardControl(){
  char *strValue = new char[256];
  String cad = "";

  printf("component KeyboardControl?(y/n): ");
  gets(strValue);
  while (String(strValue).Lower() != "y" && String(strValue).Lower() != "n") {
    printf("\tPlease, type 'y' or 'n': ");
    gets(strValue);
  }
  if (String(strValue).Lower() == "y"){
    uint32 right, left, down, up;
    printf("\tright(eInputCode): ");
    scanf_s("%d", &right);
    gets(strValue);

    printf("\tleft(eInputCode): ");
    scanf_s("%d", &left);
    gets(strValue);

    printf("\tdown(eInputCode): ");
    scanf_s("%d", &down);
    gets(strValue);

    printf("\tup(eInputCode): ");
    scanf_s("%d", &up);
    gets(strValue);

    cad += "\t\t<KeyboardControl right=\"" + String::FromInt(right) + "\" left=\"" + String::FromInt(left) + "\" down=\"" + String::FromInt(down) + "\" up=\"" + String::FromInt(up) + "\"/>\n";
  }

  delete strValue;
  return cad;
}

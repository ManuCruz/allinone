#include "../includeGame/ecs.h"
#include "../includeGame/game.h"
#include "../includeGame/gameOverMenu.h"

//COMPONENT-----------------------
Entity* Component::getOwner() {
  return m_Owner;
}

//POSITION
C_PositionWithBoundary::C_PositionWithBoundary(Entity* owner, double x, double y, double minX, double maxX, double minY, double maxY) : Component(owner) {
  if (minX < maxX){
    m_minX = minX;
    m_maxX = maxX;
  }
  else{
    m_minX = maxX;
    m_maxX = minX;
  }

  if (minY < maxY){
    m_minY = minY;
    m_maxY = maxY;
  }
  else{
    m_minY = maxY;
    m_maxY = minY;
  }

  if (x < m_minX)
    m_x = m_minX;
  else if (x > m_maxX)
    m_x = m_maxX;
  else
    m_x = x;

  if (y < m_minY)
    m_y = m_minY;
  else if (y > m_maxY)
    m_y = m_maxY;
  else
    m_y = y;

  M_SetPosition *mes = new M_SetPosition();
  mes->x = m_x;
  mes->y = m_y;

  getOwner()->ReceiveMessage(mes);

  delete mes;
}

void C_PositionWithBoundary::Update(double elapsed) {
  M_SetPosition *mes = new M_SetPosition();
  mes->x = m_x;
  mes->y = m_y;

  getOwner()->ReceiveMessage(mes);

  delete mes;
}

void C_PositionWithBoundary::ReceiveMessage(Message *msg) {
  M_MovementMessage *mes = dynamic_cast<M_MovementMessage*>(msg);
  if (mes){
    m_x += mes->amountX;
    m_y += mes->amountY;

    if (m_x < m_minX){
      m_x = m_minX;

      M_ChangeDirectionX *mesX = new M_ChangeDirectionX();
      getOwner()->ReceiveMessage(mesX);
      delete mesX;
    }
    else if (m_x > m_maxX){
      m_x = m_maxX;

      M_ChangeDirectionX *mesX = new M_ChangeDirectionX();
      getOwner()->ReceiveMessage(mesX);
      delete mesX;
    }

    if (m_y < m_minY){
      m_y = m_minY;

      M_ChangeDirectionY *mesY = new M_ChangeDirectionY();
      getOwner()->ReceiveMessage(mesY);
      delete mesY;
    }
    else if (m_y > m_maxY){
      m_y = m_maxY;

      M_ChangeDirectionY *mesY = new M_ChangeDirectionY();
      getOwner()->ReceiveMessage(mesY);
      delete mesY;
    }
  }

  M_SetPosition *mes2 = dynamic_cast<M_SetPosition*>(msg);
  if (mes2){
    m_x = mes2->x;
    m_y = mes2->y;

    if (m_x < m_minX)
      m_x = m_minX;
    else if (m_x > m_maxX)
      m_x = m_maxX;

    if (m_y < m_minY)
      m_y = m_minY;
    else if (m_y > m_maxY)
      m_y = m_maxY;
  }

  M_GetPosition *mes3 = dynamic_cast<M_GetPosition*>(msg);
  if (mes3){
    mes3->x = m_x;
    mes3->y = m_y;
  }
}

//MOVEMENT
void C_Movement::Update(double elapsed) {
  M_MovementMessage *mes = new M_MovementMessage();
  mes->amountX = m_speedX * elapsed * m_dirX;
  mes->amountY = m_speedY * elapsed * m_dirY;

  getOwner()->ReceiveMessage(mes);

  delete mes;
}

void C_Movement::ReceiveMessage(Message *msg) {
  M_SetDirection *mes = dynamic_cast<M_SetDirection*>(msg);
  if (mes){
    m_dirX = mes->dirX;
    m_dirY = mes->dirY;
  }
}

//COUNTER
void C_Counter::Update(double elapsed){
}

void C_Counter::ReceiveMessage(Message *msg) {
  M_IncreaseCounter *mes = dynamic_cast<M_IncreaseCounter*>(msg);
  if (mes)
    m_count++;

  M_DecreaseCounter *mes2 = dynamic_cast<M_DecreaseCounter*>(msg);
  if (mes2)
    m_count--;

  M_GetCount *mes3 = dynamic_cast<M_GetCount*>(msg);
  if (mes3)
    mes3->count = m_count;
}

//RENDERABLE
void C_Renderable::Update(double elapsed){
}

void C_Renderable::ReceiveMessage(Message *msg){
  M_SetPosition *mes1 = dynamic_cast<M_SetPosition*>(msg);
  if (mes1)
    m_Sprite->SetPosition(mes1->x, mes1->y);

  M_SetCollisionable *mes3 = dynamic_cast<M_SetCollisionable*>(msg);
  if (mes3)
    mes3->answer = m_Sprite->GetCollision() ? true : false;

  M_DidCollided *mes4 = dynamic_cast<M_DidCollided*>(msg);
  if (mes4)
    mes4->answer = m_Sprite->DidCollide();

  M_GetSprite *mes5 = dynamic_cast<M_GetSprite*>(msg);
  if (mes5)
    mes5->spr = m_Sprite;
}

//KEYBOARD_CONTROL
C_KeyboardControl::C_KeyboardControl(Entity* owner, eInputCode right, eInputCode left, eInputCode down, eInputCode up) : Component(owner){
  INPUT.CreateActionAxis("ejeX" + String::FromInt(owner->getId()), right, left);
  INPUT.CreateActionAxis("ejeY" + String::FromInt(owner->getId()), up, down);
}

void C_KeyboardControl::Update(double elapsed){
  int8 dirX = 0, dirY = 0;

  if (INPUT.GetActionAxis("ejeX" + String::FromInt(getOwner()->getId())) > 0)
    dirX = +1;
  if (INPUT.GetActionAxis("ejeX" + String::FromInt(getOwner()->getId())) < 0)
    dirX = -1;
  if (INPUT.GetActionAxis("ejeY" + String::FromInt(getOwner()->getId())) > 0)
    dirY = -1;
  if (INPUT.GetActionAxis("ejeY" + String::FromInt(getOwner()->getId())) < 0)
    dirY = +1;

  M_SetDirection *mes = new M_SetDirection();
  mes->dirX = dirX;
  mes->dirY = dirY;

  getOwner()->ReceiveMessage(mes);

  delete mes;
}

void C_KeyboardControl::ReceiveMessage(Message *msg) {
}

//COLLISIONABLE
C_Collisionable::C_Collisionable(Entity* owner) : Component(owner) {
  M_SetCollisionable *mes = new M_SetCollisionable();

  getOwner()->ReceiveMessage(mes);

  collionable = mes->answer;
  
  delete mes;
}
void C_Collisionable::Update(double elapsed) {
}

void C_Collisionable::ReceiveMessage(Message *msg) {
  M_HasCollision *mes2 = dynamic_cast<M_HasCollision*>(msg);
  if (mes2 && collionable){
    M_DidCollided *mes3 = new M_DidCollided();
    getOwner()->ReceiveMessage(mes3);
    mes2->answer = mes3->answer;

    delete mes3;
  }

  M_Status *mes4 = dynamic_cast<M_Status*>(msg);
  if (mes4){
    M_DidCollided *mes5 = new M_DidCollided();
    getOwner()->ReceiveMessage(mes5);
    mes4->collided = mes5->answer;

    delete mes5;
  }
}

//PICKABLE
void C_Pickable::Update(double elapsed){
}
void C_Pickable::ReceiveMessage(Message *msg){
  M_IsPickable *mes = dynamic_cast<M_IsPickable*>(msg);
  if (mes)
    mes->answer = true;

  M_Status *mes2 = dynamic_cast<M_Status*>(msg);
  if (mes2)
    mes2->pickable = true;

  M_CollisionWith *mes3 = dynamic_cast<M_CollisionWith*>(msg);
  if (mes3){
    M_Status *mes4 = new M_Status();
    mes3->ent->ReceiveMessage(mes4);

    //comprobar estados y determininar acciones
    if (mes4->mainChar){
      M_SetPosition *mesNewPos = new M_SetPosition();
      mesNewPos->x = rand() % Screen::Instance().GetWidth();
      mesNewPos->y = rand() % Screen::Instance().GetHeight();
      getOwner()->ReceiveMessage(mesNewPos);

      delete mesNewPos;
    }

    if (mes4->impenetrable){
      M_GetPrePosition *mesPre = new M_GetPrePosition();
      getOwner()->ReceiveMessage(mesPre);

      M_SetPosition *mesNewPos = new M_SetPosition();
      mesNewPos->x = mesPre->x;
      mesNewPos->y = mesPre->y;
      getOwner()->ReceiveMessage(mesNewPos);

      delete mesPre;
      delete mesNewPos;
    }

    delete mes4;
  }
}

//MAINCHAR
void C_MainChar::Update(double elapsed){
}
void C_MainChar::ReceiveMessage(Message *msg){
  M_IsMainChar *mes = dynamic_cast<M_IsMainChar*>(msg);
  if (mes)
    mes->answer = true;

  M_Status *mes2 = dynamic_cast<M_Status*>(msg);
  if (mes2)
    mes2->mainChar = true;

  M_CollisionWith *mes3 = dynamic_cast<M_CollisionWith*>(msg);
  if (mes3){
    M_Status *mes4 = new M_Status();
    mes3->ent->ReceiveMessage(mes4);

    //comprobar estados y determininar acciones
    if (mes4->pickable && !mes4->enemy){
      M_IncreaseCounter *mesCount = new M_IncreaseCounter();
      getOwner()->ReceiveMessage(mesCount);
      delete mesCount;
    }
  
    if (mes4->enemy)
      Game::Instance()->gameOver(getOwner());

    if (mes4->impenetrable){
      M_GetPrePosition *mesPre = new M_GetPrePosition();
      getOwner()->ReceiveMessage(mesPre);
      
      M_SetPosition *mesNewPos = new M_SetPosition();
      mesNewPos->x = mesPre->x;
      mesNewPos->y = mesPre->y;
      getOwner()->ReceiveMessage(mesNewPos);

      delete mesPre;
      delete mesNewPos;
    }

    if (mes4->goal){
      GameOverMenu::Instance()->setTextMenu(LOC("victory"));
      GameOverMenu::setNextState(GameOverMenu::Instance());
    }

    delete mes4;
  }
}

//ENEMY
void C_Enemy::Update(double elapsed){
}
void C_Enemy::ReceiveMessage(Message *msg){
  M_IsEnemy *mes = dynamic_cast<M_IsEnemy*>(msg);
  if (mes)
    mes->answer = true;

  M_Status *mes2 = dynamic_cast<M_Status*>(msg);
  if (mes2)
    mes2->enemy = true;

  M_CollisionWith *mes3 = dynamic_cast<M_CollisionWith*>(msg);
  if (mes3){
    M_Status *mes4 = new M_Status();
    mes3->ent->ReceiveMessage(mes4);

    if (mes4->impenetrable){
      M_GetPrePosition *mesPre = new M_GetPrePosition();
      getOwner()->ReceiveMessage(mesPre);

      M_SetPosition *mesNewPos = new M_SetPosition();
      mesNewPos->x = mesPre->x;
      mesNewPos->y = mesPre->y;
      getOwner()->ReceiveMessage(mesNewPos);

      delete mesPre;
      delete mesNewPos;
    }

    delete mes4;
  }
}

//AUTOMOVE
C_AutoMove::C_AutoMove(Entity* owner, double speedX, double speedY) : Component(owner) {
  m_speedX = speedX;
  m_speedY = speedY;
}
void C_AutoMove::Update(double elapsed){
  M_MovementMessage *mes = new M_MovementMessage();
  mes->amountX = m_speedX * elapsed;
  mes->amountY = m_speedY * elapsed;

  getOwner()->ReceiveMessage(mes);

  delete mes;
}
void C_AutoMove::ReceiveMessage(Message *msg){
  M_ChangeDirectionX *mesX = dynamic_cast<M_ChangeDirectionX*>(msg);
  if (mesX)
    m_speedX *= -1;

  M_ChangeDirectionY *mesY = dynamic_cast<M_ChangeDirectionY*>(msg);
  if (mesY)
    m_speedY *= -1;
}

//IMPENETRABLE
void C_Impenetrable::Update(double elapsed){
}
void C_Impenetrable::ReceiveMessage(Message *msg){
  M_SetPosition *mes1 = dynamic_cast<M_SetPosition*>(msg);
  if (mes1){
    m_preX = m_posX;
    m_posX = mes1->x;
    m_preY = m_posY;
    m_posY = mes1->y;
  }

  M_IsImpenetrable *mes2 = dynamic_cast<M_IsImpenetrable*>(msg);
  if (mes2)
    mes2->answer = true;

  M_Status *mes3 = dynamic_cast<M_Status*>(msg);
  if (mes3)
    mes3->impenetrable = true;

  M_GetPrePosition *mes4 = dynamic_cast<M_GetPrePosition*>(msg);
  if (mes4){
    mes4->x = m_preX;
    mes4->y = m_preY;
  }
}

//GOAL
void C_Goal::Update(double elapsed){
}
void C_Goal::ReceiveMessage(Message *msg){
  M_IsGoal *mesGoal = dynamic_cast<M_IsGoal*>(msg);
  if (mesGoal)
    mesGoal->answer = true;

  M_Status *mesSta = dynamic_cast<M_Status*>(msg);
  if (mesSta)
    mesSta->goal = true;
}


//ENTITY-------------------------------------
Entity::~Entity(){
  for (uint32 i = 0; i < m_Components.Size(); i++)
    delete m_Components[i];
}

void Entity::Update(double elapsed) {
  for (uint32 i = 0; i < m_Components.Size(); i++)
    m_Components[i]->Update(elapsed);
}

void Entity::ReceiveMessage(Message *msg){
  for (uint32 i = 0; i < m_Components.Size(); i++)
    m_Components[i]->ReceiveMessage(msg);
}


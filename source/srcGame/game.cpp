#include "../includeGame/game.h"

#include "../include/screen.h"
#include "../include/renderer.h"
#include "../include/resourceManager.h"
#include "../include/image.h"
#include "../include/emitter.h"
#include "../include/filemanager.h"
#include "../include/math.h"
#include "../include/inputManager.h"

#include "../includeGame/inGameMenu.h"
#include "../includeGame/gameOverMenu.h"
#include "../includeGame/customGameMenu.h"

#define SCR Screen::Instance()
#define REN Renderer::Instance()
#define RES ResourceManager::Instance()
#define INPUT InputManager::Instance()

#include "../lib/rapidxml.hpp"
using namespace rapidxml;

Game* Game::game = NULL;

Game::Game(){
  effects = true;
  countDown = 3;
  tempCount = 0.;
  fileConfig = "";
}

Game::~Game(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->destroy();
    delete controls[i];
  }
  controls.Clear();

  delete scene;
}

void Game::init(){
  //delete previous Match
  if (scene){
    aEntities.Clear();
    delete scene;
    controls.Clear();
  }

  controls.Add(new Window());
  ((Window*)controls.Last())->init("Ventana", NULL, NULL);

  controls.Add(new Label());
  ((Label*)controls.Last())->init("l_count", Vector2(-1, 250), "", Menu::getFont(), 255, 0, 255, NULL, controls[0]);

  GUI.setRootControl(controls[0]);

  //new match  
  scene = new Scene();
  scene->GetCamera().SetBounds(0., 0., SCR.GetWidth(), SCR.GetHeight());

  String file = FileManager::Instance().LoadString(fileConfig);

  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* nGame = doc.first_node("game");

    if (nGame->first_attribute("timeToWin"))
      timeToWin = String(nGame->first_attribute("timeToWin")->value()).ToInt();
    else
      timeToWin = 0.;

    if (nGame->first_attribute("emitter")){
      //particle emitter
      Image *iSpr = RES.LoadImage(String(nGame->first_attribute("emitter")->value()));
      iSpr->SetMidHandle();
      emitter = scene->CreateEmitter(iSpr, true);
      emitter->SetAngularVelocity(720., 720.);
      emitter->SetLifetime(1., 2.);
      emitter->SetRate(500., 1000.);
      emitter->SetVelocityX(-64., 64.);
      emitter->SetVelocityY(-64., 64.);
      emitter->SetMaxColor(255, 0, 0);
    }
    else
      emitter = NULL;

    xml_node<>* nEntity = nGame->first_node("entity");
    while (nEntity){
      Entity ent;
      aEntities.Add(ent);

      //component Renderable
      xml_node<>* nRenderable = nEntity->first_node("Renderable");
      if (nRenderable){
        Image *iSpr = RES.LoadImage(String(nRenderable->first_attribute("sprite")->value()));
        iSpr->SetMidHandle();
        Sprite *spr = scene->CreateSprite(iSpr, scene->LAYER_FRONT);
        spr->SetCollision(Sprite::COLLISION_RECT);
        int32 r = String(nRenderable->first_attribute("r")->value()).ToInt();
        int32 g = String(nRenderable->first_attribute("g")->value()).ToInt();
        int32 b = String(nRenderable->first_attribute("b")->value()).ToInt();
        spr->SetColor((uint8)r, (uint8)g, (uint8)b);
        aEntities.Last().AddComponent(new C_Renderable(&aEntities.Last(), spr));
      }

      //component PositionWithBoundary
      xml_node<>* nPositionWithBoundary = nEntity->first_node("PositionWithBoundary");
      if (nPositionWithBoundary){
        double minX = String(nPositionWithBoundary->first_attribute("minX")->value()).ToFloat();
        double maxX = String(nPositionWithBoundary->first_attribute("maxX")->value()).ToFloat();
        double minY = String(nPositionWithBoundary->first_attribute("minY")->value()).ToFloat();
        double maxY = String(nPositionWithBoundary->first_attribute("maxY")->value()).ToFloat();

        double x = getRandValue(minX, maxX);
        double y = getRandValue(minY, maxY);

        double bX0 = nPositionWithBoundary->first_attribute("bX0")? String(nPositionWithBoundary->first_attribute("bX0")->value()).ToFloat() : 0;
        double bX1 = nPositionWithBoundary->first_attribute("bX1") ? String(nPositionWithBoundary->first_attribute("bX1")->value()).ToFloat() : SCR.GetWidth();
        double bY0 = nPositionWithBoundary->first_attribute("bY0") ? String(nPositionWithBoundary->first_attribute("bY0")->value()).ToFloat() : 0;
        double bY1 = nPositionWithBoundary->first_attribute("bY1") ? String(nPositionWithBoundary->first_attribute("bY1")->value()).ToFloat() : SCR.GetHeight();

        aEntities.Last().AddComponent(new C_PositionWithBoundary(&aEntities.Last(), x, y, bX0, bX1, bY0, bY1));
      }

      //component Movement
      xml_node<>* nMovement = nEntity->first_node("Movement");
      if (nMovement){
        double minSpeedX = String(nMovement->first_attribute("minSpeedX")->value()).ToFloat();
        double maxSpeedX = String(nMovement->first_attribute("maxSpeedX")->value()).ToFloat();
        double minSpeedY = String(nMovement->first_attribute("minSpeedY")->value()).ToFloat();
        double maxSpeedY = String(nMovement->first_attribute("maxSpeedY")->value()).ToFloat();

        double speedX = getRandValue(minSpeedX, maxSpeedX);
        double speedY = getRandValue(minSpeedY, maxSpeedY);

        aEntities.Last().AddComponent(new C_Movement(&aEntities.Last(), speedX, speedY));
      }

      //component Counter
      xml_node<>* nCounter = nEntity->first_node("Counter");
      if (nCounter){
        aEntities.Last().AddComponent(new C_Counter(&aEntities.Last()));
      }

      //component KeyboardControl
      xml_node<>* nKeyboardControl = nEntity->first_node("KeyboardControl");
      if (nKeyboardControl){
        eInputCode r = (eInputCode)String(nKeyboardControl->first_attribute("right")->value()).ToInt();;
        eInputCode l = (eInputCode)String(nKeyboardControl->first_attribute("left")->value()).ToInt();;
        eInputCode d = (eInputCode)String(nKeyboardControl->first_attribute("down")->value()).ToInt();;
        eInputCode u = (eInputCode)String(nKeyboardControl->first_attribute("up")->value()).ToInt();;

        aEntities.Last().AddComponent(new C_KeyboardControl(&aEntities.Last(), r, l, d, u));
      }

      //component Collisionable
      xml_node<>* nCollisionable = nEntity->first_node("Collisionable");
      if (nCollisionable){
        aEntities.Last().AddComponent(new C_Collisionable(&aEntities.Last()));
      }

      //component MainChar
      xml_node<>* nMainChar = nEntity->first_node("MainChar");
      if (nMainChar){
        aEntities.Last().AddComponent(new C_MainChar(&aEntities.Last()));
      }

      //component Pickable
      xml_node<>* nPickable = nEntity->first_node("Pickable");
      if (nPickable){
        aEntities.Last().AddComponent(new C_Pickable(&aEntities.Last()));
      }

      //component Enemy
      xml_node<>* nEnemy = nEntity->first_node("Enemy");
      if (nEnemy){
        aEntities.Last().AddComponent(new C_Enemy(&aEntities.Last()));
      }

      //component AutoMove
      xml_node<>* nAutoMove = nEntity->first_node("AutoMove");
      if (nAutoMove){
        double minSpeedX = String(nAutoMove->first_attribute("minSpeedX")->value()).ToFloat();
        double maxSpeedX = String(nAutoMove->first_attribute("maxSpeedX")->value()).ToFloat();
        double minSpeedY = String(nAutoMove->first_attribute("minSpeedY")->value()).ToFloat();
        double maxSpeedY = String(nAutoMove->first_attribute("maxSpeedY")->value()).ToFloat();

        double speedX = getRandValue(minSpeedX, maxSpeedX);
        double speedY = getRandValue(minSpeedY, maxSpeedY);

        aEntities.Last().AddComponent(new C_AutoMove(&aEntities.Last(), speedX, speedY));
      }

      //component Impenetrable
      xml_node<>* nImpenetrable = nEntity->first_node("Impenetrable");
      if (nImpenetrable){
        aEntities.Last().AddComponent(new C_Impenetrable(&aEntities.Last()));
      }

      //component Goal
      xml_node<>* nGoal = nEntity->first_node("Goal");
      if (nGoal){
        aEntities.Last().AddComponent(new C_Goal(&aEntities.Last()));
      }

      //next entity
      nEntity = nEntity->next_sibling("entity");
    }

    //setting
    countTime = 0.;
    isPlaying = true;
    tempCount = 0.;
  }
  else{ //wrong name
    SCR.setText("");
    setNextState(CustomGameMenu::Instance());
  }
}

void Game::run(){
  GUI.setRootControl(controls[0]);
  double elapsed = SCR.ElapsedTime();

  //countDown to start
  if (countDown > tempCount){
    tempCount += elapsed;
    ((Label*)controls[1])->setText(String::FromInt((int32)(countDown - tempCount + 1)));
  }
  else{
    ((Label*)controls[1])->setText("");
    if (isPlaying){
      countTime += elapsed;

      //update all entities
      for (uint32 i = 0; i < aEntities.Size(); i++)
        aEntities[i].Update(elapsed);

      //update all
      scene->Update(elapsed);

      //collision
      for (uint32 i = 0; i < aEntities.Size() - 1; i++){
        M_Status *mes1 = new M_Status();
        aEntities[i].ReceiveMessage(mes1);
        if (mes1->collided){//si la entidad ha colisionado, recupero su sprite y pregunto a las siguientes entidades
          M_GetSprite *mesSpr1 = new M_GetSprite();
          aEntities[i].ReceiveMessage(mesSpr1);
          for (uint32 j = i + 1; j < aEntities.Size(); j++){
            M_Status *mes2 = new M_Status();
            aEntities[j].ReceiveMessage(mes2);
            if (mes2->collided){//si la segunda entidad ha colisionado, recupero su sprite
              M_GetSprite *mesSpr2 = new M_GetSprite();
              aEntities[j].ReceiveMessage(mesSpr2);
              if (mesSpr1->spr->CheckCollision(mesSpr2->spr)){ //si han colisionado entre ellas, analizar el posible efecto
                M_CollisionWith *mesCol1 = new M_CollisionWith();
                M_CollisionWith *mesCol2 = new M_CollisionWith();

                mesCol1->ent = &aEntities[j];
                mesCol2->ent = &aEntities[i];

                aEntities[i].ReceiveMessage(mesCol1);
                aEntities[j].ReceiveMessage(mesCol2);
                delete mesCol1;
                delete mesCol2;
              }
              delete mesSpr2;
            }
            delete mes2;
          }
          delete mesSpr1;
        }
        delete mes1;
      }

      //victory
      if (timeToWin != 0. && countTime > timeToWin){
        int32 countPJ = 1;
        int32 pj = 1;
        int32 countF = -1;
        for (uint32 i = 0; i < aEntities.Size(); i++){
          M_Status *mesSta = new M_Status();
          aEntities[i].ReceiveMessage(mesSta);
          if (mesSta->mainChar){
            M_GetCount *mes = new M_GetCount();
            aEntities[i].ReceiveMessage(mes);
            if (mes->count >= 0){
              if (mes->count > countF){
                countF = mes->count;
                pj = countPJ;
              }
              countPJ++;
            }
            delete mes;
          }
          delete mesSta;
        }

        GameOverMenu::Instance()->setTextMenu(LOC("victory") + " PJ" + String::FromInt(pj));
        setNextState(GameOverMenu::Instance());
      }
    }
    else{
      scene->Update(elapsed);
      if (effects && emitter)
        emitter->Stop();
      setNextState(GameOverMenu::Instance());
    }

    //render all
    scene->Render();

    //SCORE
    renderScore();

    if (INPUT.IsActionUp("exit")){
      setNextState(InGameMenu::Instance());
      setPreviousState(this);
    }
  }
}

void Game::end(){
}

void Game::renderScore(){
  float y = 15;
  Font * f = Menu::getFont();
  int32 iE = (int32)countTime;
  int32 iD = (int32)((countTime - iE) * 100);

  if (controls.Size() == 2){
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_time", Vector2(15, y), LOC("time") + ": " + String::FromInt(iE) + "." + String::FromInt(iD), f, 255, 0, 255, NULL, controls[0]);
  }
  else
    ((Label*)controls[2])->setText(LOC("time") + ": " + String::FromInt(iE) + "." + String::FromInt(iD));

  y += (float)(f->GetSize()*1.5);

  int32 countPJ = 1;
  for (uint32 i = 0; i < aEntities.Size(); i++){
    M_Status *mesSta = new M_Status();
    aEntities[i].ReceiveMessage(mesSta);
    if (mesSta->mainChar){
      M_GetCount *mes = new M_GetCount();
      aEntities[i].ReceiveMessage(mes);
      if (mes->count >= 0){
        if (controls.Size() == 3 + i){
          controls.Add(new Label());
          ((Label*)controls.Last())->init("l_point" + String::FromInt(countPJ), Vector2(15, y), LOC("points") + " P" + String::FromInt(countPJ) + ": " + String::FromInt(mes->count), f, 255, 0, 255, NULL, controls[0]);
        }
        else
          ((Label*)controls[3 + i])->setText(LOC("points") + " P" + String::FromInt(countPJ) + ": " + String::FromInt(mes->count));
        countPJ++;
        y += (float)(f->GetSize()*1.5);
      }
      delete mes;
    }
    delete mesSta;
  }
}

void Game::gameOver(Entity* ent){
  isPlaying = false;
  GameOverMenu::Instance()->setTextMenu(LOC("gameOver"));

  if (effects && emitter){
    M_GetPosition *mesPos = new M_GetPosition();
    ent->ReceiveMessage(mesPos);
    emitter->SetPosition(mesPos->x, mesPos->y);
    delete mesPos;

    emitter->Start();
  }
}

double Game::getRandValue(double value1, double value2){
  if (value1 < value2)
    return value1 + WrapValue(rand(), value2 - value1);
  else if (value1 == value2)
    return value1;
  else
    return value2 + WrapValue(rand(), value1 - value2);
}
#include "../includeGame/gameOverMenu.h"
#include "../includeGame/initialMenu.h"
#include "../includeGame/game.h"

class gameOverListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_restart")
      GameOverMenu::Instance()->setOption(restart);
    else if (sender->getName() == "b_mainMenu")
      GameOverMenu::Instance()->setOption(mainMenu);
  }
};

gameOverListener goListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
GameOverMenu* GameOverMenu::gameOverMenu = NULL;

void GameOverMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(115, 150), "data/GUI/Window2.png", &goListener, Game::Instance()->getRootControl());

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(15, 6), textMenu.Upper(), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_restart", Vector2(-1, 100), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &goListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_restart", Vector2(-1, 6), LOC("restart"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_mainMenu", Vector2(-1, 150), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &goListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_mainMenu", Vector2(-1, 6), LOC("mainMenu"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(Game::Instance()->getRootControl());
  option = none;
}

void GameOverMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(15, 6), textMenu.Upper());
  ((Label*)controls[3])->reset(Vector2(-1, 6), LOC("restart"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("mainMenu"));
}

void GameOverMenu::run(){
  Game::Instance()->getScene()->Update(SCR.ElapsedTime());
  Game::Instance()->getScene()->Render();
  Game::Instance()->renderScore();

  switch (option){
  case restart:
    setNextState(Game::Instance());
    break;
  case mainMenu:
    setNextState(InitialMenu::Instance());
    break;
  }

  if (INPUT.IsActionUp("exit"))
    setNextState(InitialMenu::Instance());
}

void GameOverMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
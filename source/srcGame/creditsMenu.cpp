#include "../includeGame/creditsMenu.h"
#include "../includeGame/initialMenu.h"
#include "../includeGame/exitMenu.h"

class creditsListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_mainMenu")
      CreditsMenu::Instance()->setOption(mainMenu);
  }
};

creditsListener cListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
CreditsMenu* CreditsMenu::creditsMenu = NULL;

void CreditsMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(0., 0.), "data/GUI/Window4.png", &cListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(50, 20), LOC("credits"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_creditsText", Vector2(-1, 150), LOC("creditsText"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_author", Vector2(-1, 200), LOC("author"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_mainMenu", Vector2(-1, 300), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &cListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_mainMenu", Vector2(-1, 6), LOC("mainMenu"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void CreditsMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(50, 20), LOC("credits"));
  ((Label*)controls[2])->reset(Vector2(-1, 150), LOC("creditsText"));
  ((Label*)controls[3])->reset(Vector2(-1, 200), LOC("author"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("mainMenu"));
}

void CreditsMenu::run(){
  switch (option){
  case mainMenu:
    setNextState(InitialMenu::Instance());
    break;
  }

  if (INPUT.IsActionUp("exit"))
    setNextState(InitialMenu::Instance());
}

void CreditsMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
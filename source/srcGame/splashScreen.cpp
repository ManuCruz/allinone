#include "../includeGame/splashScreen.h"
#include "../includeGame/initialMenu.h"

SplashScreen* SplashScreen::splashScreen = NULL;

void SplashScreen::init(){
  control = new Window();
  ((Window*)control)->init("Ventana", Vector2(0., 0.), "data/GUI/splash.png", NULL, NULL);

  GUI.setRootControl(control);
}

void SplashScreen::run(){
  double elapsed = SCR.ElapsedTime();

  if (time - 1 > tempTime){
    uint8 value = (tempTime / (time - 1)) * 255;
    REN.SetColor(value, value, value, value);
  }
  else if (time < tempTime)
    setNextState(InitialMenu::Instance());

  tempTime += elapsed;
}

void SplashScreen::end(){
  control->destroy();
}

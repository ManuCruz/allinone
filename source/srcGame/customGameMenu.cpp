#include "../includeGame/customGameMenu.h"
#include "../includeGame/selectGameMenu.h"
#include "../includeGame/initialMenu.h"
#include "../includeGame/game.h"

class customGameListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_start")
      CustomGameMenu::Instance()->setOption(start);
    else if (sender->getName() == "b_clear")
      CustomGameMenu::Instance()->setOption(clear);
    else if (sender->getName() == "b_mainMenu")
      CustomGameMenu::Instance()->setOption(mainMenu);
    else if (sender->getName() == "b_back")
      CustomGameMenu::Instance()->setOption(back);
  }
};

customGameListener cgListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
CustomGameMenu* CustomGameMenu::customGameMenu = NULL;

void CustomGameMenu::init(){
  SCR.setText("");
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(0., 0.), "data/GUI/Window4.png", &cgListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(50, 20), LOC("custom"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_name", Vector2(-1, 100), SCR.getText(), font, 0, 0, 255, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_start", Vector2(-1, 200), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &cgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_start", Vector2(-1, 6), LOC("start"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_clear", Vector2(-1, 250), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &cgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_clear", Vector2(-1, 6), LOC("clear"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_mainMenu", Vector2(-1, 300), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &cgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_mainMenu", Vector2(-1, 6), LOC("mainMenu"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_back", Vector2(-1, 350), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &cgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_back", Vector2(-1, 6), LOC("back"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void CustomGameMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(50, 20), LOC("custom"));
  ((Label*)controls[4])->reset(Vector2(-1, 6), LOC("start"));
  ((Label*)controls[6])->reset(Vector2(-1, 6), LOC("clear"));
  ((Label*)controls[8])->reset(Vector2(-1, 6), LOC("mainMenu"));
  ((Label*)controls[10])->reset(Vector2(-1, 6), LOC("back"));
}

void CustomGameMenu::run(){
  fileName = SCR.getText();

  ((Label*)controls[2])->setText(fileName);
  Vector2 vec;
  vec.x = (float)(((((Label*)controls[2])->getParent())->getSize().x - font->GetTextWidth(fileName)) / 2.);
  vec.y = (float)100.;
  ((Label*)controls[2])->setPosition(vec);
 
  switch (option){
  case start:
    Game::Instance()->setFileConfig(fileName);
    setNextState(Game::Instance());
    break;
  case clear:
    fileName = "";
    SCR.setText(fileName);
    option = none;
    break;
  case mainMenu:
    setNextState(InitialMenu::Instance());
    break;
  case back:
    setNextState(SelectGameMenu::Instance());
    break;
  }

  if (INPUT.IsActionDown("enter")){
    Game::Instance()->setFileConfig(fileName);
    setNextState(Game::Instance());
  }

  if (INPUT.IsActionUp("exit"))
    setNextState(SelectGameMenu::Instance());
}

void CustomGameMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
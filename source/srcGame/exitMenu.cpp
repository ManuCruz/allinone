#include "../includeGame/exitMenu.h"

class exitListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_yes")
      ExitMenu::Instance()->setOption(yes);
    else if (sender->getName() == "b_no")
      ExitMenu::Instance()->setOption(no);
  }
};

exitListener eListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
ExitMenu* ExitMenu::exitMenu = NULL;

void ExitMenu::init(){
  Control* parent = ((Menu*)getPreviousState())->getRootControl();
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(115, 225), "data/GUI/Window1.png", &eListener, parent);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(-1, 20), LOC("quit") + "?", font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_yes", Vector2(30, 50), "data/GUI/Button_Normal4.png", "data/GUI/Button_Push4.png", &eListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_yes", Vector2(-1, 6), LOC("yes"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_no", Vector2(150, 50), "data/GUI/Button_Normal4.png", "data/GUI/Button_Push4.png", &eListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_no", Vector2(-1, 6), LOC("no"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(parent);
  option = none;
}

void ExitMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(-1, 20), LOC("quit") + "?");
  ((Label*)controls[3])->reset(Vector2(-1, 6), LOC("yes"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("no"));
}

void ExitMenu::run(){

  switch (option){
  case yes:
    setNextState(NULL);
    break;
  case no:
    setNextState(getPreviousState());
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  }
}

void ExitMenu::end(){

}
#include "../includeGame/optionMenu.h"
#include "../includeGame/game.h"

class optionListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_en")
      OptionMenu::Instance()->setOption(en);
    else if (sender->getName() == "b_es")
      OptionMenu::Instance()->setOption(es);
    else if (sender->getName() == "c_effects"){
      if (((CheckBox*)sender)->getActived())
        OptionMenu::Instance()->setOption(enable);
      else
        OptionMenu::Instance()->setOption(disable);
    }
    else if (sender->getName() == "b_back")
      OptionMenu::Instance()->setOption(back);
  }
};

optionListener oListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
OptionMenu* OptionMenu::optionMenu = NULL;

void OptionMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(0., 0.), "data/GUI/Window4.png", &oListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(50, 20), LOC("options").Upper(), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_effects", Vector2(100, 200), LOC("effects"), font, 0, 0, 255, NULL, controls[0]);
    controls.Add(new CheckBox());
    ((CheckBox*)controls.Last())->init("c_effects", Vector2((float)font->GetTextWidth(LOC("effects")) + 10, -7), "data/GUI/CheckBox_enabled.png", "data/GUI/CheckBox_disabled.png", &oListener, controls[controls.Size() - 2]);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_countDown", Vector2(-1, 250), LOC("countDown") + ": " + String::FromInt(Game::Instance()->getCountDown()), font, 0, 0, 255, NULL, controls[0]);
    controls.Add(new Slider());
    ((Slider*)controls.Last())->init("s_countDown", Vector2(-1, 300), "data/GUI/Slider_bar.png", "data/GUI/Slider_ball.png", (float)Game::Instance()->getCountDown(), 1., 10., 1., &oListener, controls[0], true);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_en", Vector2(60, 350), "data/GUI/Button_Normal3.png", "data/GUI/Button_Push3.png", &oListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_en", Vector2(-1, 6), LOC("en"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_es", Vector2(260, 350), "data/GUI/Button_Normal3.png", "data/GUI/Button_Push3.png", &oListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_es", Vector2(-1, 6), LOC("es"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_back", Vector2(-1, 400), "data/GUI/Button_Normal3.png", "data/GUI/Button_Push3.png", &oListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_back", Vector2(-1, 6), LOC("back"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void OptionMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(50, 20), LOC("options").Upper());
  ((Label*)controls[2])->reset(Vector2(100, 200), LOC("effects"));
  ((Label*)controls[4])->reset(Vector2(-1, 250), LOC("countDown") + ": " + String::FromInt(Game::Instance()->getCountDown()));
  ((Label*)controls[7])->reset(Vector2(-1, 6), LOC("en"));
  ((Label*)controls[9])->reset(Vector2(-1, 6), LOC("es"));
  ((Label*)controls[11])->reset(Vector2(-1, 6), LOC("back"));
}

void OptionMenu::run(){
  if (Game::Instance()->getCountDown() != ((Slider*)controls[5])->getValue()){
    Game::Instance()->setCountDown((int32)((Slider*)controls[5])->getValue());
    ((Label*)controls[4])->setText(LOC("countDown") + ": " + String::FromInt(Game::Instance()->getCountDown()));
  }

  switch (Menu::option){
  case enable:
    Game::Instance()->enableEffects();
    setNextState(OptionMenu::Instance());
    break;
  case disable:
    Game::Instance()->disableEffects();
    setNextState(OptionMenu::Instance());
    break;
  case en:
    LocalizationManager::Instance().SetLanguage("en");
    setCurrentState(NULL);
    setNextState(OptionMenu::Instance());
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  case es:
    LocalizationManager::Instance().SetLanguage("es");
    setCurrentState(NULL);
    setNextState(OptionMenu::Instance());
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  case back:
    setNextState(getPreviousState());
    break;
  }

  if (INPUT.IsActionUp("exit"))
    setNextState(getPreviousState());
}

void OptionMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
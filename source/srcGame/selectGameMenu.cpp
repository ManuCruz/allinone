#include "../includeGame/selectGameMenu.h"
#include "../includeGame/initialMenu.h"
#include "../includeGame/game.h"
#include "../includeGame/customGameMenu.h"

class selectGameListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_sSurvival")
      SelectGameMenu::Instance()->setOption(sSurvival);
    else if (sender->getName() == "b_mFruits")
      SelectGameMenu::Instance()->setOption(mFruits);
    else if (sender->getName() == "b_versus")
      SelectGameMenu::Instance()->setOption(versus);
    else if (sender->getName() == "b_frog")
      SelectGameMenu::Instance()->setOption(frog);
    else if (sender->getName() == "b_custom")
      SelectGameMenu::Instance()->setOption(custom);
    else if (sender->getName() == "b_back")
      SelectGameMenu::Instance()->setOption(back);
  }
};

selectGameListener sgListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
SelectGameMenu* SelectGameMenu::selectGameMenu = NULL;

void SelectGameMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(0., 0.), "data/GUI/Window4.png", &sgListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(50, 20), LOC("select"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_sSurvival", Vector2(-1, 200), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_sSurvival", Vector2(-1, 6), LOC("sSurvival"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_mFruits", Vector2(-1, 250), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_mFruits", Vector2(-1, 6), LOC("mFruits"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_versus", Vector2(-1, 300), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_versus", Vector2(-1, 6), LOC("versus"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_frog", Vector2(-1, 350), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_frog", Vector2(-1, 6), LOC("frog"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_custom", Vector2(-1, 400), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_custom", Vector2(-1, 6), LOC("custom"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_back", Vector2(-1, 450), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &sgListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_back", Vector2(-1, 6), LOC("back"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void SelectGameMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(50, 20), LOC("select"));
  ((Label*)controls[3])->reset(Vector2(-1, 6), LOC("sSurvival"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("mFruits"));
  ((Label*)controls[7])->reset(Vector2(-1, 6), LOC("versus"));
  ((Label*)controls[9])->reset(Vector2(-1, 6), LOC("frog"));
  ((Label*)controls[11])->reset(Vector2(-1, 6), LOC("custom"));
  ((Label*)controls[13])->reset(Vector2(-1, 6), LOC("back"));
}

void SelectGameMenu::run(){
  switch (Menu::option){
  case sSurvival:
    Game::Instance()->setFileConfig("data/games/survival1.xml");
    setNextState(Game::Instance());
    break;
  case mFruits:
    Game::Instance()->setFileConfig("data/games/competitiveFruits.xml");
    setNextState(Game::Instance());
    break;
  case versus:
    Game::Instance()->setFileConfig("data/games/versus.xml");
    setNextState(Game::Instance());
    break;
  case frog:
    Game::Instance()->setFileConfig("data/games/frog.xml");
    setNextState(Game::Instance());
    break;
  case custom:
    setNextState(CustomGameMenu::Instance());
    break;
  case back:
    setNextState(InitialMenu::Instance());
    break;
  }

  if (INPUT.IsActionUp("exit"))
    setNextState(InitialMenu::Instance());
}

void SelectGameMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
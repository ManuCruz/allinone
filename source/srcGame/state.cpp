#include "../includeGame/state.h"
#include "stdio.h"

State *State::currentState = NULL;
State *State::nextState = NULL;
State *State::previousState = NULL;

State* State::getCurrentState(){
  return currentState;
}

State* State::getNextState(){
  return nextState;
}

State* State::getPreviousState(){
  return previousState;
}

void State::setCurrentState(State *current){
  currentState = current;
}

void State::setNextState(State *next){
  nextState = next;
}

void State::setPreviousState(State *previous){
  previousState = previous;
}
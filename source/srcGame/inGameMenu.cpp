#include "../includeGame/inGameMenu.h"
#include "../includeGame/game.h"
#include "../includeGame/initialMenu.h"
#include "../includeGame/optionMenu.h"

class inGameListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_cont")
      InGameMenu::Instance()->setOption(cont);
    else if (sender->getName() == "b_restart")
      InGameMenu::Instance()->setOption(restart);
    else if (sender->getName() == "b_options")
      InGameMenu::Instance()->setOption(options);
    else if (sender->getName() == "b_mainMenu")
      InGameMenu::Instance()->setOption(mainMenu);
  }
};

inGameListener igListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
InGameMenu* InGameMenu::inGameMenu = NULL;

void InGameMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(115, 150), "data/GUI/Window2.png", &igListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(20, 6), LOC("pause"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_cont", Vector2(-1, 50), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &igListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_cont", Vector2(-1, 6), LOC("cont"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_restart", Vector2(-1, 100), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &igListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_restart", Vector2(-1, 6), LOC("restart"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_options", Vector2(-1, 150), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &igListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_options", Vector2(-1, 6), LOC("options"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_mainMenu", Vector2(-1, 200), "data/GUI/Button_Normal2.png", "data/GUI/Button_Push2.png", &igListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_mainMenu", Vector2(-1, 6), LOC("mainMenu"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void InGameMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(20, 6), LOC("pause"));
  ((Label*)controls[3])->reset(Vector2(-1, 6), LOC("cont"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("restart"));
  ((Label*)controls[7])->reset(Vector2(-1, 6), LOC("options"));
  ((Label*)controls[9])->reset(Vector2(-1, 6), LOC("mainMenu"));
}

void InGameMenu::run(){
  Game::Instance()->getScene()->Render();

  switch (option){
  case cont:
    setCurrentState(Game::Instance());
    setNextState(Game::Instance());
    break;
  case restart:
    setNextState(Game::Instance());
    break;
  case options:
    setNextState(OptionMenu::Instance());
    setPreviousState(this);
    break;
  case mainMenu:
    setNextState(InitialMenu::Instance());
    break;
  }

  if (INPUT.IsActionUp("exit")){
    setCurrentState(Game::Instance());
    setNextState(Game::Instance());
  }
}

void InGameMenu::end(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->setVisible(false);
  }
}
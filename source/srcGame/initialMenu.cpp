#include "../includeGame/initialMenu.h"
#include "../includeGame/optionMenu.h"
#include "../includeGame/creditsMenu.h"
#include "../includeGame/selectGameMenu.h"
#include "../includeGame/exitMenu.h"

class initialListener : public IEventListener{
  void onClick(Control* sender)	{
    if (sender->getName() == "b_play")
      InitialMenu::Instance()->setOption(play);
    else if (sender->getName() == "b_options")
      InitialMenu::Instance()->setOption(options);
    else if (sender->getName() == "b_credits")
      InitialMenu::Instance()->setOption(credits);
    else if (sender->getName() == "b_quit")
      InitialMenu::Instance()->setOption(quit);
  }
};

initialListener iListener;

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
InitialMenu* InitialMenu::initialMenu = NULL;

void InitialMenu::init(){
  if (controls.Size() == 0){
    controls.Add(new Window());
    ((Window*)controls.Last())->init("Ventana", Vector2(0., 0.), "data/GUI/Window4.png", &iListener, NULL);

    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_title", Vector2(50, 20), LOC("title"), font, 230, 95, 0, NULL, controls[0]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_play", Vector2(-1, 200), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &iListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_play", Vector2(-1, 6), LOC("play"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_options", Vector2(-1, 250), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &iListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_options", Vector2(-1, 6), LOC("options"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_credits", Vector2(-1, 300), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &iListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_credits", Vector2(-1, 6), LOC("credits"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);

    controls.Add(new Button());
    ((Button*)controls.Last())->init("b_quit", Vector2(-1, 350), "data/GUI/Button_Normal.png", "data/GUI/Button_Push.png", &iListener, controls[0]);
    controls.Add(new Label());
    ((Label*)controls.Last())->init("l_quit", Vector2(-1, 6), LOC("quit"), font, 0, 0, 255, NULL, controls[controls.Size() - 2]);
  }
  else{
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(true);
    }
    relabel();
  }
  GUI.setRootControl(controls[0]);
  option = none;
}

void InitialMenu::relabel(){
  ((Label*)controls[1])->reset(Vector2(50, 20), LOC("title"));
  ((Label*)controls[3])->reset(Vector2(-1, 6), LOC("play"));
  ((Label*)controls[5])->reset(Vector2(-1, 6), LOC("options"));
  ((Label*)controls[7])->reset(Vector2(-1, 6), LOC("credits"));
  ((Label*)controls[9])->reset(Vector2(-1, 6), LOC("quit"));
}

void InitialMenu::run(){
  switch (option){
  case play:
    setNextState(SelectGameMenu::Instance());
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  case options:
    setNextState(OptionMenu::Instance());
    setPreviousState(this);
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  case credits:
    setNextState(CreditsMenu::Instance());
    for (uint32 i = 0; i < controls.Size(); i++){
      controls[i]->setVisible(false);
    }
    break;
  case quit:
    setNextState(ExitMenu::Instance());
    setPreviousState(this);
    break;
  case none:
    break;
  }

  if (INPUT.IsActionUp("exit")){
    setNextState(ExitMenu::Instance());
    setPreviousState(this);
  }
}

void InitialMenu::end(){
}

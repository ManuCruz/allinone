#ifndef _CUSTOMGAME_MENU_H_
#define _CUSTOMGAME_MENU_H_

#include "menu.h"

class CustomGameMenu : public Menu {
public:
  static CustomGameMenu* Instance() { if (!customGameMenu) customGameMenu = new CustomGameMenu(); return customGameMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  CustomGameMenu() {}
  ~CustomGameMenu() {}

  static CustomGameMenu* customGameMenu;
  String fileName;
};

#endif

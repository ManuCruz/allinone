#ifndef _SELECTGAME_MENU_H_
#define _SELECTGAME_MENU_H_

#include "menu.h"

class SelectGameMenu : public Menu {
public:
  static SelectGameMenu* Instance() { if (!selectGameMenu) selectGameMenu = new SelectGameMenu(); return selectGameMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  SelectGameMenu() {}
  ~SelectGameMenu() {}

  static SelectGameMenu* selectGameMenu;
};

#endif

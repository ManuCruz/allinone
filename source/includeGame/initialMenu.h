#ifndef _INITIAL_MENU_H_
#define _INITIAL_MENU_H_

#include "menu.h"

class InitialMenu : public Menu {
public:
  static InitialMenu* Instance() { if (!initialMenu) initialMenu = new InitialMenu(); return initialMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  InitialMenu() {}
  ~InitialMenu();

  static InitialMenu* initialMenu;
};

#endif

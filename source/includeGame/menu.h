#ifndef _MENU_H_
#define _MENU_H_

#include "state.h"
#include "../include/types.h"
#include "../include/array.h"
#include "../include/string.h"
#include "../include/font.h"

#include "../lib/glfw.h"

#include "../include/screen.h"
#define SCR Screen::Instance()

#include "../include/localizationmanager.h"
#define LOC(s) LocalizationManager::Instance().GetString(s)

#include "../include/renderer.h"
#define REN Renderer::Instance()

#include "../include/inputManager.h"
#define INPUT InputManager::Instance()

#include "../uGui/GuiManager.h"
#define GUI GuiManager::Instance()
#include "../uGui/Window.h"
#include "../uGui/Button.h"
#include "../uGui/Label.h"
#include "../uGui/CheckBox.h"
#include "../uGui/Slider.h"

enum opt {
  play,
  options,
  credits,
  quit,
  sSurvival,
  mFruits,
  versus,
  frog,
  custom,
  back,
  countDown,
  cont,
  restart,
  mainMenu,
  start,
  clear,
  en,
  es,
  enable,
  disable,
  yes,
  no,
  none,
};

class Menu : public State {
public:
  static void setFont(Font *f) { font = f; }
  static Font* getFont() { return font; }

  virtual void init() = 0;
  virtual void run() = 0;
  virtual void end() = 0;

  void setOption(opt option){ this->option = option; }

  Control* getRootControl() { return controls[0]; };

  ~Menu();

  virtual void relabel() = 0;

protected:
  static Font *font;
  opt option;

  Array<Control*> controls;
};

#endif

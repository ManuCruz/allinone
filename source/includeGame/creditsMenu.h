#ifndef _CREDITS_MENU_H_
#define _CREDITS_MENU_H_

#include "menu.h"

class CreditsMenu : public Menu {
public:
  static CreditsMenu* Instance() { if (!creditsMenu) creditsMenu = new CreditsMenu(); return creditsMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  CreditsMenu() {}
  ~CreditsMenu() {}

  static CreditsMenu* creditsMenu;
};

#endif

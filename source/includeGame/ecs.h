#ifndef _ECS_H
#define _ECS_H

#include "../include/array.h"
#include "../include/types.h"
#include "../include/inputManager.h"
#include "../include/sprite.h"

class Entity;

//MESSAGES-------------------------
class Message{
public:
  virtual ~Message() {};
};

class M_MovementMessage : public Message{
public:
  double amountX = 0.;
  double amountY = 0.;
};

class M_IncreaseCounter : public Message{
};

class M_DecreaseCounter : public Message{
};

class M_SetPosition : public Message{
public:
  double x = 0.;
  double y = 0.;
};

class M_GetPosition : public Message{
public:
  double x = 0.;
  double y = 0.;
};

class M_SetDirection : public Message{
public:
  int8 dirX = 0;
  int8 dirY = 0;
};

class M_GetCount : public Message{
public:
  int32 count = -1;
};

class M_SetCollisionable : public Message{
public:
  bool answer = false;
};

class M_HasCollision : public Message{
public:
  bool answer = false;
};

class M_DidCollided : public Message{
public:
  bool answer = false;
};

class M_GetSprite : public Message{
public:
  Sprite *spr = NULL;
};

class M_IsPickable : public Message{
public:
  bool answer = false;
};

class M_IsMainChar : public Message{
public:
  bool answer = false;
};

class M_IsEnemy : public Message{
public:
  bool answer = false;
};

class M_IsImpenetrable : public Message{
public:
  bool answer = false;
};

class M_IsGoal : public Message{
public:
  bool answer = false;
};

class M_Status : public Message{
public:
  bool collided = false;
  bool pickable = false;
  bool mainChar = false;
  bool enemy = false;
  bool impenetrable = false;
  bool goal = false;
};

class M_CollisionWith : public Message{
public:
  Entity *ent = NULL;
};

class M_ChangeDirectionX : public Message{
};

class M_ChangeDirectionY : public Message{
};

class M_GetPrePosition : public Message{
public:
  double x = 0.;
  double y = 0.;
};

//COMPONENT-----------------------
class Component{
  Entity* m_Owner;
public:
  Component(Entity* owner) { m_Owner = owner; }
  virtual void Update(double elapsed) = 0;
  virtual void ReceiveMessage(Message *msg) = 0;
  Entity* getOwner();
};

class C_PositionWithBoundary : public Component{
  double m_x, m_y;
  double m_minX, m_maxX;
  double m_minY, m_maxY;
public:
  C_PositionWithBoundary(Entity* owner, double x, double y, double minX, double maxX, double minY, double maxY);
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Movement : public Component{
  double m_speedX, m_speedY;
  int8 m_dirX = 1, m_dirY = 1;
public:
  C_Movement(Entity* owner, double speedX, double speedY) : Component(owner)  { m_speedX = speedX; m_speedY = speedY; }
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Counter : public Component{
  uint32 m_count;
public:
  C_Counter(Entity* owner, uint32 count = 0) : Component(owner)  { m_count = count; }
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Renderable : public Component{
  Sprite* m_Sprite;
public:
  C_Renderable(Entity* owner, Sprite *sprite) : Component(owner)  { m_Sprite = sprite; }
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_KeyboardControl : public Component{
public:
  C_KeyboardControl(Entity* owner, eInputCode right, eInputCode left, eInputCode down, eInputCode up);
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Collisionable : public Component{
  bool collionable = false;
public:
  C_Collisionable(Entity* owner);
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Pickable : public Component{
public:
  C_Pickable(Entity* owner) : Component(owner) {}
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_MainChar : public Component{
public:
  C_MainChar(Entity* owner) : Component(owner) {}
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Enemy : public Component{
public:
  C_Enemy(Entity* owner) : Component(owner) {}
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_AutoMove : public Component{
  double m_speedX, m_speedY;
public:
  C_AutoMove(Entity* owner, double speedX = 0., double speedY = 0.);
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Impenetrable : public Component{
  double m_preX, m_preY;
  double m_posX, m_posY;
public:
  C_Impenetrable(Entity* owner) : Component(owner) {}
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

class C_Goal : public Component{
public:
  C_Goal(Entity* owner) : Component(owner) {}
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

//ENTITY----------------------------------
static uint32 id_entity = 0;
class Entity{
  Array<Component*> m_Components;
  uint32 id;
public:
  Entity(){ id = id_entity, id_entity++; }
  virtual ~Entity();
  virtual void Update(double elapsed);
  void AddComponent(Component* comp) { m_Components.Add(comp); }
  void ReceiveMessage(Message *msg);
  uint32 getId() { return id; }
};

#endif
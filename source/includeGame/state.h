#ifndef _STATE_H_
#define _STATE_H_

class State{
public:
  virtual void init() = 0;
  virtual void run() = 0;
  virtual void end() = 0;

  static State* getCurrentState();
  static State* getNextState();
  static State* getPreviousState();

  static void setCurrentState(State *current);
  static void setNextState(State *next);
  static void setPreviousState(State *previous);

private:
  static State *currentState;
  static State *nextState;
  static State *previousState;
};

#endif
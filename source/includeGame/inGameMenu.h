#ifndef _INGAME_MENU_H_
#define _INGAME_MENU_H_

#include "menu.h"

class InGameMenu : public Menu {
public:
  static InGameMenu* Instance() { if (!inGameMenu) inGameMenu = new InGameMenu(); return inGameMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  InGameMenu() {}
  ~InGameMenu() {}

  static InGameMenu* inGameMenu;
};

#endif

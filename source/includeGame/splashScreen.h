#ifndef _SPLASH_SCRREN_H_
#define _SPLASH_SCRREN_H_

#include "state.h"
#include "../include/types.h"
#include "../include/array.h"
#include "../uGui/GuiManager.h"

class SplashScreen : public State {
public:
  static SplashScreen* Instance() { if (!splashScreen) splashScreen = new SplashScreen(); return splashScreen; }

  virtual void init();
  virtual void run();
  virtual void end();

private:
  SplashScreen() { time = 5; }
  ~SplashScreen() {}

  static SplashScreen* splashScreen;

  int32 time;
  double tempTime;
  Control* control;
};

#endif

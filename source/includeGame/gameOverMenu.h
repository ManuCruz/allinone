#ifndef _GAMEOVER_MENU_H_
#define _GAMEOVER_MENU_H_

#include "menu.h"

class GameOverMenu : public Menu {
public:
  static GameOverMenu* Instance() { if (!gameOverMenu) gameOverMenu = new GameOverMenu(); return gameOverMenu; }

  void init();
  void run();
  void end();

  void relabel();

  void setTextMenu(String t) { textMenu = t; }

private:
  GameOverMenu() {}
  ~GameOverMenu() {}

  static GameOverMenu* gameOverMenu;

  String textMenu;
};

#endif

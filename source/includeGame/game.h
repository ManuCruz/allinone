#ifndef _GAME_H_
#define _GAME_H_

#include "state.h"
#include "../include/types.h"
#include "../include/array.h"
#include "../include/scene.h"

#include "../lib/glfw.h"

#include "../include/screen.h"
#define SCR Screen::Instance()

#include "../include/localizationmanager.h"
#define LOC(s) LocalizationManager::Instance().GetString(s)

#include "ecs.h"

#include "../uGui/GuiManager.h"
#define GUI GuiManager::Instance()

class Game : public State {
public:
  static Game* Instance() { if (!game) game = new Game(); return game; }

  virtual void init();
  virtual void run();
  virtual void end();

  Scene* getScene() { return scene; }
  void renderScore();
  void gameOver(Entity* ent);
  void enableEffects() { effects = true; }
  void disableEffects() { effects = false; }

  int32 getCountDown() { return countDown; }
  void setCountDown(int32 CD) { countDown = CD; }

  void setFileConfig(String file) { fileConfig = file; }

  Control* getRootControl() { return controls[0]; };

private:
  Game();
  ~Game();

  double getRandValue(double value1, double value2);

  static Game* game;

  Scene *scene;
  Emitter *emitter;
  Array<Entity> aEntities;
  double countTime;
  bool isPlaying;
  double timeToWin;
  bool effects;
  int32 countDown;
  double tempCount;

  String fileConfig;
  Array<Control*> controls;
};

#endif

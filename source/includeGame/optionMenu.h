#ifndef _OPTION_MENU_H_
#define _OPTION_MENU_H_

#include "menu.h"

class OptionMenu : public Menu {
public:
  static OptionMenu* Instance() { if (!optionMenu) optionMenu = new OptionMenu(); return optionMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  OptionMenu() {}
  ~OptionMenu() {}

  static OptionMenu* optionMenu;
};

#endif

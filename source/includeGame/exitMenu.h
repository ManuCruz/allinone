#ifndef _GAMEOVER_MENU_H_
#define _GAMEOVER_MENU_H_

#include "menu.h"

class ExitMenu : public Menu {
public:
  static ExitMenu* Instance() { if (!exitMenu) exitMenu = new ExitMenu(); return exitMenu; }

  void init();
  void run();
  void end();

  void relabel();

private:
  ExitMenu() {}
  ~ExitMenu() {}

  static ExitMenu* exitMenu;
};

#endif

#include "SliderButton.h"
#include "GuiManager.h"
#include "../include/Renderer.h"
#include "../include/Image.h"
#include "../include/Math.h"
#include "../include/ResourceManager.h"
#include "Slider.h"

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
SliderButton::SliderButton(){
  m_normalImage = NULL;
	m_pushed = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool SliderButton::init(const String& name, const Vector2& position, const String& normalImage, IEventListener* eventListener, Control* parent){
  m_name = name;
  m_normalImage = ResourceManager::Instance().LoadImage(normalImage);
  m_size = Vector2((float)m_normalImage->GetWidth(), (float)m_normalImage->GetHeight());

  m_position = position;
  if (m_position.x == -1)
    m_position.x = (float)((parent->getSize().x - m_normalImage->GetWidth()) / 2.);

	setEventListener( eventListener );
	setParent( parent );

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void SliderButton::update(){
  Vector2 pos = getPosition();
  float value = ((Slider*)getParent())->getValue();
  float step = ((Slider*)getParent())->getStep();

  if (pos.x > ((Slider*)getParent())->getOffsetX(value + step / 2))
    ((Slider*)getParent())->increaseValue();
  else if (pos.x < ((Slider*)getParent())->getOffsetX(value - step / 2))
    ((Slider*)getParent())->decreaseValue();

  if (!m_pointerIsOver){
    m_pushed = false;
    ((Slider*)getParent())->setPositionBullet();
  }
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void SliderButton::render(){
	if( m_visible )	{
		Vector2 pos = getAbsolutePosition();

		Renderer::Instance().SetBlendMode( Renderer::ALPHA );

  	Renderer::Instance().DrawImage( m_normalImage, pos.x, pos.y );
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool SliderButton::onInputEvent(const MessageGui& message){
	switch( message.type ){
	case mtPointerButtonDown:
    m_pushed = true;
    return true;
		break;

	case mtPointerButtonUp:
		m_pushed = false;
    ((Slider*)getParent())->setPositionBullet();
    return true;
		break;

  case mtPointerMove:
    if (m_pushed){
      const MessagePointerMove* messagePointer = static_cast<const MessagePointerMove*>(&message);
      Vector2 absPos = getAbsolutePosition();
      
      float semiWidth = m_normalImage->GetWidth() / 2;
      
      float xParent = ((Slider*)getParent())->getAbsolutePosition().x;
      float xSizeParent = ((Slider*)getParent())->getSize().x;

      if (ValueInRange(absPos.x, xParent - semiWidth, xParent + xSizeParent - semiWidth)){
        Vector2 pos = getPosition();

        pos.x += messagePointer->x - absPos.x;
        pos.x -= semiWidth;

        setPosition(pos);
      }
    }
    return true;
    break;
	}

  return false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void SliderButton::destroy(){
  m_normalImage = NULL;
}
#ifndef _MESSAGE_H_
#define _MESSAGE_H_

enum eMessageGuiType{
	mtPointerMove,
	mtPointerButtonDown,
	mtPointerButtonUp,
	mtKeyDown,
	mtKeyUp
};

struct MessageGui{
  eMessageGuiType type;
};

struct MessagePointerMove : public MessageGui{
	MessagePointerMove() { 
		type = mtPointerMove; 
	}

	MessagePointerMove( float _x, float _y) { 
		type = mtPointerMove;
		x = _x;
		y = _y;
	}

	float x;
	float y;
};

struct MessagePointerButtonDown : public MessageGui{
	MessagePointerButtonDown() { 
		type = mtPointerButtonDown; 
	}

	MessagePointerButtonDown( int _buttonId, float _x, float _y ) { 
		type = mtPointerButtonDown;
		buttonId = _buttonId;
		x = _x;
		y = _y;
	}

	int buttonId;
	float x;
	float y;
};

struct MessagePointerButtonUp : public MessageGui{
	MessagePointerButtonUp() { 
		type = mtPointerButtonUp; 
	}

	MessagePointerButtonUp( int _buttonId, float _x, float _y ) { 
		type = mtPointerButtonUp; 
		buttonId = _buttonId;
		x = _x;
		y = _y;
	}

	int buttonId;
	float x;
	float y;
};

struct MessageKeyDown : public MessageGui{
	MessageKeyDown() { 
		type = mtKeyDown; 
	}

	MessageKeyDown( int _keyCode ) { 
		type = mtKeyDown; 
		keyCode = _keyCode;
	}

	int keyCode;
};

struct MessageKeyUp : public MessageGui{
	MessageKeyUp() { 
		type = mtKeyUp; 
	}

	MessageKeyUp( int _keyCode ) { 
		type = mtKeyUp; 
		keyCode = _keyCode;
	}

	int keyCode;
};

#endif // _MESSAGE_H_

#include "Label.h"
#include "GuiManager.h"
#include "../include/Renderer.h"
#include "../include/Font.h"

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
Label::Label(){
  m_font = NULL;
  m_text = "";
  m_r = 0;
  m_g = 0;
  m_b = 0;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Label::init(const String& name, const Vector2& position, const String text, Font* font, uint8 r, uint8 g, uint8 b, IEventListener* eventListener, Control* parent){
	m_name = name;
  m_text = text;
  m_font = font;
  m_r = r;
  m_g = g;
  m_b = b;
  
  m_size = Vector2((float)m_font->GetTextWidth(text), (float)m_font->GetTextHeight(text));

  m_position = position;
  if (m_position.x == -1)
    m_position.x = (float)((parent->getSize().x - m_font->GetTextWidth(text)) / 2.);

	setEventListener( eventListener );
	setParent( parent );

	return true;
}

void Label::reset(const Vector2& position, const String text){
  m_text = text;

  m_size = Vector2((float)m_font->GetTextWidth(text), (float)m_font->GetTextHeight(text));

  m_position = position;
  if (m_position.x < 0)
    m_position.x = (float)((m_parent->getSize().x - m_font->GetTextWidth(text)) / 2.);
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Label::update(){
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Label::render(){
	if( m_visible ){
		Vector2 pos = getAbsolutePosition();

    Renderer::Instance().SetColor(m_r, m_g, m_b, 255);
    m_font->Render(m_text, pos.x, pos.y);
    Renderer::Instance().SetColor(255, 255, 255, 255);
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Label::onInputEvent(const MessageGui& message){
  return false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Label::destroy(){
  m_font = NULL;
}

void Label::setText(const String text){
  m_text = text;
}
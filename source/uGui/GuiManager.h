#ifndef _GUI_MANAGER_H_
#define _GUI_MANAGER_H_

#include "Singleton.h"
#include "MessageGui.h"
#include "Vector2.h"
#include "../include/String.h"

class Control;

class GuiManager : public Singleton<GuiManager> {
public:  
	int init();
	void end();

	void update();
	void render();

	void setRootControl( Control* control );
	Control* getRootControl();

  void injectInput(const MessageGui& message);

	Vector2 getScreenSize() const;

	Control* findControlByName( const String& name );
	void deleteControl( const String& name );

protected:
	Control* m_rootControl;
};

#endif
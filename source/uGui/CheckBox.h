#ifndef _CHECKBOX_H_
#define _CHECKBOX_H_

#include "Button.h"

class Image;

class CheckBox : public Button {
public:
  CheckBox();

	virtual void render();
  virtual bool onInputEvent(const MessageGui& message);

  bool getActived() { return m_actived; }

protected:
  bool m_actived;
};

#endif // _CHECKBOX_H_
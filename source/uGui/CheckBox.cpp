#include "CheckBox.h"
#include "../include/Renderer.h"

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
CheckBox::CheckBox() : Button(){
  m_actived = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void CheckBox::render(){
	if( m_visible ){
		Vector2 pos = getAbsolutePosition();

		Renderer::Instance().SetBlendMode( Renderer::ALPHA );
    if (m_actived)
			Renderer::Instance().DrawImage( m_pushImage, pos.x, pos.y );
    else if (!m_actived)
			Renderer::Instance().DrawImage( m_normalImage, pos.x, pos.y );
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool CheckBox::onInputEvent(const MessageGui& message){
	switch( message.type ){
	case mtPointerButtonDown:
		m_pushed = true;
    return true;
		break;

	case mtPointerButtonUp:
    if (m_pushed){
      NOTIFY_LISTENERS(onClick(this));
      m_actived = m_actived ? false : true;
    }
		m_pushed = false;
    return true;
		break;
	}
  return false;
}
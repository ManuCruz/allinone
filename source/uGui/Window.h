#ifndef _WINDOW_H_
#define _WINDOW_H_

#include "Control.h"
#include "../include/Image.h"

class Window : public Control{
public:
	Window();

	bool init( const String name, IEventListener* eventListener, Control* parent );
	bool init( const String name, const Vector2& position, const String& backgroungImage, IEventListener* eventListener, Control* parent );

	void update();
	void render();
  bool onInputEvent(const MessageGui& message);
	void destroy();

protected:
	Image* m_canvas;
};

#endif // _WINDOW_H_
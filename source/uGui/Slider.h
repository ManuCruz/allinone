#ifndef _SLIDER_H_
#define _SLIDER_H_

#include "Control.h"
#include "../include/string.h"

class Image;
class Button;
class SliderButton;
class SliderListener;

class Slider : public Control {
  friend class SliderButton;
public:
  Slider();

  bool init(const String name, const Vector2& position, const String& barImage, const String& bulletImage, float initialValue, float minValue, float maxValue, float step, IEventListener* eventListener, Control* parent, bool arrow, const String& LNImage = "", const String& LPImage = "", const String& RNImage = "", const String& RPImage = "");

	virtual void update();
	virtual void render();
  virtual bool onInputEvent(const MessageGui& message);
	virtual void destroy();

  float getValue() { return m_value; }
  void increaseValue();
  void decreaseValue();
  void setPositionBullet();

protected:
  float getOffsetX(float value);
  float getStep() { return m_step; }

	Image* m_barImage;
	Image* m_bulletImage;
  SliderButton* m_sbBullet;

  float m_value;
  float m_minValue, m_maxValue;
  float m_step;

  bool m_pushedBar;

  Button* m_bLeft;
  Button* m_bRight;

  SliderListener* sListener;
};

#endif // _SLIDER_H_